import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';
import {Location} from '@angular/common';
import { LanguageService } from '../../../service/language.service';
declare var $: any;

@Component({
  selector: 'app-club-news-details',
  templateUrl: './club-news-details.component.html',
  styleUrls: ['./club-news-details.component.css'],
  
})
export class ClubNewsDetailsComponent implements OnInit {
  language;
  userDetails;
  newsData;
  viewImage:boolean = false;

  constructor(
    private authService: AuthServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private confirmDialogService: ConfirmDialogService,
    private lang : LanguageService
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));

    this.route.params.subscribe(params => {
      const newsid = params['newsid'];
      this.getNewsDetails(newsid);
    });
  }

  getNewsDetails(newsid) {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'get-news-by-id/'+newsid, null)
        .subscribe(
          (respData: any) => {

            if(respData.result.groups && respData.result.groups.length > 0 && respData.result.groups[0].group.id){
              this.authService.memberSendRequest('get', 'approvedGroupUsers/group/'+respData.result.groups[0].group.id, null)
                .subscribe(
                  (resp: any) => {
                    let userId = localStorage.getItem('user-id');
                    let checkGroup = 0;
                    resp[0].participants.forEach((value, index) => {
                      if(value.user_id == userId){
                        checkGroup = 1;
                      }
                    });
                    if(checkGroup ==  1){
                      this.getFirstNews(respData);
                    }else{
                      var redirectUrl = 'clubwall/';
                      this.router.navigate([redirectUrl]);
                    }
                  }
                )
                
            }else{
              this.getFirstNews(respData);
            }            
            // console.log('------------------------------in-----------')
            // console.log(respData.result.groups[0].group.id)
            this.authService.setLoader(false);
          }
        );
    }
  }
showToggle:boolean = false;
  onShow(){
    let el = document.getElementsByClassName("bunch_drop");
    if(!this.showToggle){
      this.showToggle = true;
      el[0].className = "bunch_drop show";
    }
    else{
      this.showToggle = false;
      el[0].className = "bunch_drop";
    }
  }

  removeHtml(str) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = str;
    return tmp.textContent || tmp.innerText || "";
  }

  getFirstNews(allNews) {
    let news = allNews['result'];
    this.newsData = news;
    console.log('this.newsData')
    console.log(this.newsData)
  }

  goBack(){
    this._location.back();
  }

  deleteNews(newsId) {
    let self = this;
    this.confirmDialogService.confirmThis(this.language.confirmation_message.delete_article, function () {  
      self.authService.setLoader(true);
      self.authService.memberSendRequest('delete', 'news/'+newsId, null)
      .subscribe(
        (respData: any) => {
          self.authService.setLoader(false);
          const url: string[] = ["/clubwall"];
          self.router.navigate(url);
        }
      )
    }, function () {  
      console.log("No clicked");  
    })
  }

  updateNews(newsId) {
    const url: string[] = ["/update-news/"+newsId];
    this.router.navigate(url);
  }

}
