import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-club-news',
  templateUrl: './club-news.component.html',
  styleUrls: ['./club-news.component.css']
})
export class ClubNewsComponent implements OnInit {
  language;
  dashboardData;
  userData;
  role = '';
  guestNews = [];

  constructor(
    private authService: AuthServiceService,
    private lang : LanguageService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.userData = JSON.parse(localStorage.getItem('user-data'));
    console.log("USERDATA");
    this.role = this.userData.roles[0];
    this.getAllNews();
  }

  getAllNews() {
    if (sessionStorage.getItem('token')) {
      let userId = localStorage.getItem('user-id');
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'news/user/'+userId, null)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            this.dashboardData = respData;
            console.log("respData all news");
            console.log(respData);

            if(this.role == 'guest'){
              this.guestNews = [];

              for (const key in this.dashboardData) {
                if (Object.prototype.hasOwnProperty.call(this.dashboardData, key)) {
                  const element = this.dashboardData[key];
                  
                  if(element.show_guest_list == 'true'){
                    this.guestNews.push(element);
                  }
                }
              }

            }

            console.log("GuestNews---");
            console.log(this.guestNews);
           }
        );
    }
  }

  showAll(){
    console.log("SHOW ALL-");
    
    console.log($('.nav'));
    console.log($('.nav')[0].childNodes);
    /* $('.nav')[0].childNodes.forEach(function(val, key) {
      if(val.classList.length){
        val.classList.remove("menu_active");
        if(val.classList.value == 'active'){
          console.log("Val");
          console.log(val);
          console.log(val.classList);
          val.classList.add("menu_active");
          localStorage.setItem("menu-active", "menu_active");
        }
      }
    }); */
    this.router.navigate(['/clubwall-news/1']);
  }

  removeHtml(str) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = str;
    return tmp.textContent || tmp.innerText || "";
  }  
}
