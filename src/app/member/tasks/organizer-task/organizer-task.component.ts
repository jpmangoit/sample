import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';
declare var $: any;

@Component({
  selector: 'app-organizer-task',
  templateUrl: './organizer-task.component.html',
  styleUrls: ['./organizer-task.component.css']
})
export class OrganizerTaskComponent implements OnInit {
  language;
  displayAllTasks: boolean = true;
  displayPersonalTasks: boolean = false;
  displayGroupTasks: boolean = false;
  displayCreatedTasks: boolean = false;

  constructor(
    private authService: AuthServiceService,
    private router: Router,
    private lang : LanguageService
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
  }

  onTasks(id) {
    console.log("Task CLICKED " + id);
    $('.tab-pane').removeClass('active');
    $('.nav-link').removeClass('active');
    if (id == 1) {
      this.displayAllTasks =  true;
      this.displayPersonalTasks =  false;
      this.displayGroupTasks =  false;
      this.displayCreatedTasks =  false;
      $('#tabs-1').addClass('active');
      $('#head-allTask').addClass('active');
    }
    if (id == 2) {
      this.displayAllTasks =  false;
      this.displayPersonalTasks =  true;
      this.displayGroupTasks =  false;
      this.displayCreatedTasks =  false;
      $('#tabs-2').addClass('active');
      $('#head-personalTask').addClass('active');
    }
    if (id == 3) {
      this.displayAllTasks =  false;
      this.displayPersonalTasks =  false;
      this.displayGroupTasks =  true;
      this.displayCreatedTasks =  false;
      $('#tabs-3').addClass('active');
      $('#head-groupTask').addClass('active');
    }
    if (id == 4) {
      this.displayAllTasks =  false;
      this.displayPersonalTasks =  false;
      this.displayGroupTasks =  false;
      this.displayCreatedTasks =  true;
      $('#tabs-4').addClass('active');
      $('#head-createdTask').addClass('active');
    }
  }
}

