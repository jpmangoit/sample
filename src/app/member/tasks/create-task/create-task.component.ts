import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css'],
  providers: [DatePipe]
})
export class CreateTaskComponent implements OnInit {
  language;
  organizer_id;
  submitted = false;
  user_dropdown = [];
  group_dropdown;
  groupDropdownSettings;
  type_visibility = null;
  type_dropdown = [];
  typeDropdownSettings;
  subTaskArr;
  subtaskArray = [];
  subTaskUserDropdownSettings;
  subTaskSelectedUserToShow: boolean = false;
  subTaskSelectedUser = [];
  receiveData;
  userDetails;
  showUsers;
  participant = [];
  groups = [];
  participantSelectedItem = [];
  participantSelectedToShow = [];
  participantDropdownSettings;
  responseMessage = null;

  showSubtask: boolean = false;
  collaboratorList: FormArray;
  subtaskList: FormArray;
  createTaskForm: FormGroup;

  constructor(
    private authService: AuthServiceService,
    public formBuilder: FormBuilder,
    private router: Router,
    private lang : LanguageService
  ) { }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    maxHeight: '15rem',
    translate: 'no',
    toolbarHiddenButtons: [
      [
        'link',
        'unlink',
        'subscript',
        'superscript',
        'insertUnorderedList',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode',
        'insertImage',
        'insertVideo'
      ]
    ],
    sanitize: true,
    toolbarPosition: 'top',
    defaultFontName: 'Arial',
    defaultFontSize: '2',
    defaultParagraphSeparator: 'p'
  };

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    $('#subtask').hide();
    $('#showSubtask').hide();
    this.organizer_id = localStorage.getItem('user-id');
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));
    this.getGroup();
    this.getUser();
    this.type_dropdown = [
      { "id": "0", "name": this.language.create_task.individual },
      { "id": "1", "name": this.language.create_task.group }
    ];

    this.typeDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.subTaskUserDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'user_name',
      allowSearchFilter: true,
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.participantDropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'user_name',
      allowSearchFilter: true,
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.groupDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      allowSearchFilter: true,
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.createTaskForm = this.formBuilder.group({
      title: ['', [Validators.required, this.noWhitespace]],
      description: ['', Validators.required],
      organizer_id: [localStorage.getItem('user-id')],
      status: ['1'],
      groups:[''],
      group_id: [''],
      date: ['', Validators.required],
      type_dropdown: ['', Validators.required],
      user_participant: [''],
      collaborators: this.formBuilder.array([]),
      subtasks: this.formBuilder.array([])
    });

    // set contactlist to this field
    this.collaboratorList = this.createTaskForm.get('collaborators') as FormArray;
    this.subtaskList = this.createTaskForm.get('subtasks') as FormArray;
  }

  // returns all form groups under subtasks
  get subtaskFormGroup() {
    return this.createTaskForm.get('subtasks') as FormArray;
  }

  createSubtask(): FormGroup {
    return this.formBuilder.group({
      title: ['', Validators.compose([Validators.required, this.noWhitespace])],
      description: ['', Validators.compose([Validators.required])],
      assigned_to: ['', Validators.compose([Validators.required])],
      status: ['0'],
      date: ['', Validators.compose([Validators.required])]
    });
  }

  removeSubtask(index) {
    this.subtaskList.removeAt(index);
  }

  noWhitespace(control: FormControl) {
    if(control.value && control.value.length != 0){
      let isWhitespace = (control.value || '').trim().length === 0;
      let isValid = !isWhitespace;
      return isValid ? null : { 'whitespace': true }
    }else{
      let isValid = true;
      return isValid ? null : { 'whitespace': true }
    }
  }

  addSubtask() {
    $('#showSubtask').show();
    if ($('#showSubtask').is(":visible")) {
      this.subtaskList.push(this.createSubtask());
    }
  }

  getSubtasksFormGroup(index): FormGroup {
    const formGroup = this.subtaskList.controls[index] as FormGroup;
    return formGroup;
  }

  get collaboratorFormGroup() {
    return this.createTaskForm.get('collaborators') as FormArray;
  }

  createCollaborator(id: any): FormGroup {
    if (id == this.organizer_id) {
      return this.formBuilder.group({
        user_id: [id, Validators.compose([Validators.required])],
        approved_status: ['1', Validators.compose([Validators.required])]
      });
    }else {
      return this.formBuilder.group({
        user_id: [id, Validators.compose([Validators.required])],
        approved_status: ['0', Validators.compose([Validators.required])]
      });
    }
  }

  addCollaborator(id: any) {
    this.collaboratorList.push(this.createCollaborator(id));
  }

  removeCollaborator(index) {
    this.collaboratorList.removeAt(index);
  }

  onSubTaskUserSelect(item: any, i) {
    this.subTaskSelectedUserToShow = true;
    this.subTaskSelectedUser = item;
  }

  onSubTaskUserDeSelect(item: any) {
    this.subTaskSelectedUserToShow = false;
  }

  onUserSelect(item: any) {
    this.showUsers = true;
    this.participantSelectedToShow.push(item);
    this.participantSelectedItem.push(item.id);
    this.addCollaborator(item.id);
  }

  onUserDeSelect(item: any) {
    this.createTaskForm.get('collaborators').value.forEach((value, index) => {
      if (value.user_id == item.id) {
        this.removeCollaborator(index);
      }
    });
    this.participantSelectedToShow.forEach((value, index) => {
      if (value.id == item.id) {
        this.participantSelectedToShow.splice(index, 1);
      }
    });
    this.participantSelectedItem.forEach((value, index) => {
      if (value == item.id) {
        this.participantSelectedItem.splice(index, 1);
      }
    }); 
  }

  onGroupSelect(item: any) {
    console.log(item);
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  onTypeSelect(item: any) {
    this.type_visibility = item.id;
    this.showUsers = false;
    this.participantSelectedToShow = [];
    (<FormArray>this.createTaskForm.get('collaborators')).clear();
    this.createTaskForm.controls['user_participant'].setValue([]);
    this.createTaskForm.controls['group_id'].setValue([]);
    this.createTaskForm.controls['groups'].setValue([]);
    if (this.type_visibility == "1") {
      this.createTaskForm.get('groups').setValidators(Validators.required);
      this.createTaskForm.get('groups').updateValueAndValidity();
      this.createTaskForm.get('user_participant').clearValidators();
      this.createTaskForm.get('user_participant').updateValueAndValidity();
    }else {
      this.createTaskForm.get('user_participant').setValidators(Validators.required);
      this.createTaskForm.get('user_participant').updateValueAndValidity();
      this.createTaskForm.get('groups').clearValidators();
      this.createTaskForm.get('groups').updateValueAndValidity();
    }
  }

  onTypeDeSelect(item: any){
    this.type_visibility = null;
    this.showUsers = false;
    this.participantSelectedToShow = [];
    (<FormArray>this.createTaskForm.get('collaborators')).clear();
    this.createTaskForm.controls['user_participant'].setValue([]);
    this.createTaskForm.controls['group_id'].setValue([]);
    this.createTaskForm.controls['groups'].setValue([]);
  }

  getUser() {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'teamUsers/team/1', null)
      .subscribe(
        (respData: any) => {
          this.authService.setLoader(false);
          this.receiveData = respData;
          Object(respData).forEach((val,key) => {
            if (this.userDetails.userId != val.id){
              this.user_dropdown.push({
                'id': val.id,
                'user_email': val.email,
                'user_name': val.firstname + " " + val.lastname + " (" + val.email + " )"
              }); 
            }                 
          });
        }
      );
    }
  }

  getGroup() {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'teamgroups/1', null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            this.group_dropdown = respData;
          }
        );
    }
  }

  onCreateTask() {
    this.submitted = true;
    if (sessionStorage.getItem('token')) {
      if (this.createTaskForm.get('subtasks').value.length) {
        this.createTaskForm.get('subtasks').value.forEach((value, index) => {
          if (value.assigned_to.length) {
            var assigned = value.assigned_to[0].id;
            ((this.createTaskForm.get('subtasks') as FormArray).at(index) as FormGroup).get('assigned_to').patchValue(assigned);
          }
        });
      }

      for (const key in this.createTaskForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.createTaskForm.value, key)) {
          const element = this.createTaskForm.value[key];
          if ((element == "" || (element && element.length == 0)) && (key != "subtasks")) {
            this.createTaskForm.value[key] = null;
          }
        }
      }
      if (this.createTaskForm.valid) {
        if (this.createTaskForm.get('groups').value.length) {
          var groupId = this.createTaskForm.get('groups').value[0].id;
          this.createTaskForm.get('group_id').setValue(groupId);
          this.authService.memberSendRequest('get', 'approvedGroupUsers/group/'+groupId, null)
          .subscribe(
            (respData: any) => {
              respData[0].participants.forEach((value, index) => {
                this.addCollaborator(value.user_id);               
              });              
              this.submitTask();
            }
          );
        }else{
          this.createTaskForm.get('group_id').setValue('null');
          this.addCollaborator(this.organizer_id);
          this.submitTask();
        }
      }
    }
  }

  onCancel(){
    window.history.back();
  }

  submitTask() {
    this.authService.setLoader(true);
    this.createTaskForm.removeControl('type_dropdown');
    this.createTaskForm.removeControl('user_participant');
    this.createTaskForm.removeControl('groups');
    this.authService.memberSendRequest('post', 'createTask', this.createTaskForm.value)
    .subscribe(
      (respData: any) => {
        this.authService.setLoader(false);
        if (respData['isError'] == false) {
          var redirectUrl = 'task-detail/'+respData['result']['task']['id'];
          this.router.navigate([redirectUrl]);
        }
        if (respData['code'] == 400) {
          this.responseMessage = respData['message'];
        }
      }
    );
  }

}
