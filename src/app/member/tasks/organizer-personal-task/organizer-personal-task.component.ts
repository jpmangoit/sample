import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';
import { LanguageService } from '../../../service/language.service';
declare var $: any;

@Component({
  selector: 'app-organizer-personal-task',
  templateUrl: './organizer-personal-task.component.html',
  styleUrls: ['./organizer-personal-task.component.css']
})
export class OrganizerPersonalTaskComponent implements OnInit {
  language;
  user_id;
  personalTasks;

  constructor(
    private authService: AuthServiceService,
    private router: Router,
    private confirmDialogService: ConfirmDialogService,
    private lang : LanguageService
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.user_id = localStorage.getItem('user-id');
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'getAllApprovedTasks/user/'+this.user_id, null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            this.personalTasks = respData.reverse();
          }
        );
    }
  }

  eventMarkComplete(taskId,subtaskStatus){
    let self = this;
    if (subtaskStatus == 1){
      this.confirmDialogService.confirmThis(this.language.confirmation_message.complete_task, function () {  
        self.authService.memberSendRequest('get', 'approveTaskById/task/'+taskId, null)
        .subscribe(
          (respData: JSON) => {
            self.ngOnInit();
          }
        )
      }, function () {  
        $('#styled-checkbox-'+taskId).prop('checked', false);
      })
    }else{
      $('#styled-checkbox-'+taskId).prop('checked', false);
      $('#subtask').modal('toggle');
    }
  }

  calculateDiff(dateSent){
    let currentDate = new Date();
    dateSent = new Date(dateSent);
    return -1 * (Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()) ) /(1000 * 60 * 60 * 24)));
  }

  closeModal() {
    $('#subtask').modal('hide')
  }

}
