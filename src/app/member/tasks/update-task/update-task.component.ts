import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css'],
  providers: [DatePipe]
})
export class UpdateTaskComponent implements OnInit {
  language;
  organizer_id;
  submitted = false;
  user_dropdown = [];
  group_dropdown;
  groupDropdownSettings;
  type_visibility;
  type_dropdown = [];
  typeDropdownSettings;
  subTaskArr;
  subtaskArray = [];
  subTaskUserDropdownSettings;
  subTaskSelectedUserToShow: boolean = false;
  subTaskSelectedUser = [];
  receiveData;
  userDetails;
  showUsers;
  participant = [];
  groups = [];
  participantSelectedItem = [];
  participantSelectedToShow = [];
  participantDropdownSettings;
  responseMessage = null;
  showSubtask: boolean = false;
  collaboratorList: FormArray;
  subtaskList: FormArray;
  createTaskForm: FormGroup;
  taskDetails;
  taskid;
  setTaskUsers = [];
  types= [];

  constructor(
    private authService: AuthServiceService,
    public formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private lang: LanguageService
  ) { }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    maxHeight: '15rem',
    translate: 'no',
    toolbarHiddenButtons: [
      [
        'link',
        'unlink',
        'subscript',
        'superscript',
        'insertUnorderedList',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode',
        'insertImage',
        'insertVideo'
      ]
    ],
    sanitize: true,
    toolbarPosition: 'top',
    defaultFontName: 'Arial',
    defaultFontSize: '2',
    defaultParagraphSeparator: 'p'
  };

  ngOnInit(): void {
    let self = this;
    this.language = this.lang.getLanguaageFile();
    $('#subtask').hide();
    $('#showSubtask').hide();
    this.organizer_id = localStorage.getItem('user-id');
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));
    this.getGroup();
    this.getUser();
    this.route.params.subscribe(params => {
      this.taskid = params['taskId'];
      this.setUsers(this.taskid);      
      setTimeout(function () {
        self.setTask(self.taskid);
      }, 3000);

      // setTimeout(function () {
      //   $('.trigger_class').trigger('click');
      // }, 3000);
    });

    this.type_dropdown = [
      { "id": "0", "name": this.language.create_task.individual },
      { "id": "1", "name": this.language.create_task.group }
    ];

    this.typeDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.subTaskUserDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'user_name',
      allowSearchFilter: true,
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.participantDropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'user_name',
      allowSearchFilter: true,
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.groupDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      allowSearchFilter: true,
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.createTaskForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      organizer_id: [localStorage.getItem('user-id')],
      status: ['1'],
      groups:[''],
      group_id: [''],
      date: ['', Validators.required],
      type_dropdown: ['', Validators.required],
      user_participant: [''],
      collaborators: this.formBuilder.array([]),
      subtasks: this.formBuilder.array([])
    });

    // set contactlist to this field
    this.collaboratorList = this.createTaskForm.get('collaborators') as FormArray;
    this.subtaskList = this.createTaskForm.get('subtasks') as FormArray;
  }

  // returns all form groups under subtasks
  get subtaskFormGroup() {
    return this.createTaskForm.get('subtasks') as FormArray;
  }

  onCancel() {
    window.history.back();
  }

  createSubtask(): FormGroup {
    return this.formBuilder.group({
      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      assigned_to: ['', Validators.compose([Validators.required])],
      status: ['0'],
      date: ['', Validators.compose([Validators.required])]
    });
  }

  removeSubtask(index) {
    this.subtaskList.removeAt(index);
  }

  addSubtask() {
    $('#showSubtask').show();
    if ($('#showSubtask').is(":visible")) {
      this.subtaskList.push(this.createSubtask());
    }
  }

  getSubtasksFormGroup(index): FormGroup {
    const formGroup = this.subtaskList.controls[index] as FormGroup;
    return formGroup;
  }

  get collaboratorFormGroup() {
    return this.createTaskForm.get('collaborators') as FormArray;
  }

  createCollaborator(id: any): FormGroup {
    console.log(id+"======"+this.organizer_id)
    if (id == this.organizer_id) {
      return this.formBuilder.group({
        user_id: [id, Validators.compose([Validators.required])],
        approved_status: ['1', Validators.compose([Validators.required])]
      });
    }
    else {
      return this.formBuilder.group({
        user_id: [id, Validators.compose([Validators.required])],
        approved_status: ['0', Validators.compose([Validators.required])]
      });
    }
  }

  addCollaborator(id: any) {
    this.collaboratorList.push(this.createCollaborator(id));
  }

  removeCollaborator(index) {
    this.collaboratorList.removeAt(index);
  }

  onSubTaskUserSelect(item: any, i) {
    this.subTaskSelectedUserToShow = true;
    this.subTaskSelectedUser = item;
  }

  onSubTaskUserDeSelect(item: any) {
    this.subTaskSelectedUserToShow = false;
  }

  onUserSelect(item: any) {
    this.showUsers = true;
    this.participantSelectedToShow.push(item);
    this.participantSelectedItem.push(item.id);
    this.addCollaborator(item.id);
  }

  onUserDeSelect(item: any) {
    this.createTaskForm.get('collaborators').value.forEach((value, index) => {
      if (value.user_id == item.id) {
        this.removeCollaborator(index);
      }
    });
    this.participantSelectedToShow.forEach((value, index) => {
      if (value.id == item.id) {
        this.participantSelectedToShow.splice(index, 1);
      }
    });
    this.participantSelectedItem.forEach((value, index) => {
      if (value == item.id) {
        this.participantSelectedItem.splice(index, 1);
      }
    });
  }

  onGroupSelect(item: any) {
    (<FormArray>this.createTaskForm.get('collaborators')).clear();
    this.createTaskForm.controls['user_participant'].setValue([]);
  }

  onGroupDeSelect(item: any) {
    (<FormArray>this.createTaskForm.get('collaborators')).clear();
    this.createTaskForm.controls['user_participant'].setValue([]);
    this.createTaskForm.controls['group_id'].setValue([]);
    this.createTaskForm.controls['groups'].setValue([]);
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  onTypeSelect(item: any) {
    this.type_visibility = item.id;
    this.showUsers = false;
    this.participantSelectedToShow = [];
    (<FormArray>this.createTaskForm.get('collaborators')).clear();
    this.createTaskForm.controls['user_participant'].setValue([]);
    this.createTaskForm.controls['group_id'].setValue([]);
    this.createTaskForm.controls['groups'].setValue([]);
    if (this.type_visibility == "1") {
      this.createTaskForm.get('groups').setValidators(Validators.required);
      this.createTaskForm.get('groups').updateValueAndValidity();
      this.createTaskForm.get('user_participant').clearValidators();
      this.createTaskForm.get('user_participant').updateValueAndValidity();
    }else {
      this.createTaskForm.get('user_participant').setValidators(Validators.required);
      this.createTaskForm.get('user_participant').updateValueAndValidity();
      this.createTaskForm.get('groups').clearValidators();
      this.createTaskForm.get('groups').updateValueAndValidity();
    }
  }

  onTypeDeSelect(item: any){
    this.type_visibility = null;
    this.showUsers = false;
    this.participantSelectedToShow = [];
    (<FormArray>this.createTaskForm.get('collaborators')).clear();
    this.createTaskForm.controls['user_participant'].setValue([]);
    this.createTaskForm.controls['group_id'].setValue([]);
    this.createTaskForm.controls['groups'].setValue([]);
    console.log(this.createTaskForm.value);
  }

  getUser() {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'teamUsers/team/1', null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            this.receiveData = respData;
            for (const key in respData) {
              if (Object.prototype.hasOwnProperty.call(respData, key)) {
                var element = respData[key];
                this.user_dropdown.push({
                  'id': element.id,
                  'user_email': element.email,
                  'user_name': element.firstname + " " + element.lastname + " (" + element.email + " )"
                });
              }
            }
          }
        );
    }
  }

  getGroup() {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'teamgroups/1', null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            this.group_dropdown = respData;
          }
        );
    }
  }

  onCreateTask() {
    this.submitted = true;
    if (sessionStorage.getItem('token')) {
      if (this.createTaskForm.get('subtasks').value.length) {
        this.createTaskForm.get('subtasks').value.forEach((value, index) => {
          if (value.assigned_to.length) {
            var assigned = value.assigned_to[0].id;
            ((this.createTaskForm.get('subtasks') as FormArray).at(index) as FormGroup).get('assigned_to').patchValue(assigned);
          }
        });
      }

      for (const key in this.createTaskForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.createTaskForm.value, key)) {
          const element = this.createTaskForm.value[key];
          if ((element == "" || (element && element.length == 0)) && (key != "subtasks")) {
            this.createTaskForm.value[key] = null;
          }
        }
      }
     
      if (this.createTaskForm.valid) {
        if (this.createTaskForm.get('groups').value.length) {
          var groupId = this.createTaskForm.get('groups').value[0].id;
          this.createTaskForm.get('group_id').setValue(groupId);
          (<FormArray>this.createTaskForm.get('collaborators')).clear();
          this.authService.memberSendRequest('get', 'approvedGroupUsers/group/'+groupId, null)
          .subscribe(
            (respData: any) => {
              respData[0].participants.forEach((value, index) => {
                this.removeCollaborator(this.organizer_id);
                this.addCollaborator(value.user_id);               
              });              
              this.updateTask();
              console.log(this.createTaskForm.value);
            }
          );
        }else{
          this.createTaskForm.get('group_id').setValue('null');
          this.removeCollaborator(this.organizer_id);
          this.addCollaborator(this.organizer_id);
          this.updateTask();
        }
      }
    }
  }

  updateTask(){
    this.authService.setLoader(true);
    this.createTaskForm.removeControl('type_dropdown');
    this.createTaskForm.removeControl('user_participant');
    this.createTaskForm.removeControl('groups')
    this.authService.memberSendRequest('put', 'updateTask/'+this.taskid, this.createTaskForm.value)
    .subscribe(
      (respData: any) => {
        this.authService.setLoader(false);
        if (respData['isError'] == false) {            
            this.responseMessage = respData['result']['message'];
            var redirectUrl = 'task-detail/'+this.taskid;
            this.router.navigate([redirectUrl]);
        }
        if (respData['code'] == 400) {
          this.responseMessage = respData['message'];
        }
      }
    );
  }

  setUsers(taskid) {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'getTaskCollaborator/task/' + taskid, null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            for (const key in respData) {
              if (Object.prototype.hasOwnProperty.call(respData, key)) {
                var element = respData[key];
                if (element.user.length && element.user_id != this.organizer_id) {                 
                  this.setTaskUsers.push({
                    'id': element.user_id,
                    'user_email': element.user[0].email,
                    'user_name': element.user[0].firstname + " " + element.user[0].lastname + " (" + element.user[0].email + " )"
                  });
                  this.participantSelectedToShow.push({
                    'id': element.user_id,
                    'user_email': element.user[0].email,
                    'user_name': element.user[0].firstname + " " + element.user[0].lastname + " (" + element.user[0].email + " )"
                  }); 
                  this.addCollaborator(element.user_id);
                  this.showUsers = true;
                }
              }
            }
          }
        );
    }
  }

  setTask(taskid) {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'get-task-by-id/' + taskid, null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            this.taskDetails = respData['result'][0];
            if (this.taskDetails) {
              var task_date;
              if (this.taskDetails.date) {
                task_date = this.taskDetails.date.split("T");
              }

              if (this.taskDetails.group_id == null || this.taskDetails.group_id == 0) {
                this.type_visibility = 0;
                this.types = [{ "id": "0", "name": this.language.create_task.individual }];
                this.createTaskForm.controls['groups'].setValue('');
              }else {
                this.type_visibility = 1;
                this.types = [{ "id": "1", "name": this.language.create_task.group }];
                this.group_dropdown.forEach((value, index) => {
                  if (value.id == this.taskDetails.group_id) {
                    this.groups = [{ "id": value.id, "name": value.name }];
                    this.createTaskForm.controls['groups'].setValue(this.groups);
                    this.showUsers = false;
                  }
                });
              }
              this.createTaskForm.controls['title'].setValue(this.taskDetails.title);
              this.createTaskForm.controls['description'].setValue(this.taskDetails.description);
              this.createTaskForm.controls['organizer_id'].setValue(localStorage.getItem('user-id'));
              this.createTaskForm.controls['status'].setValue(this.taskDetails.status);
             
              this.createTaskForm.controls['date'].setValue(task_date[0]);
              this.createTaskForm.controls['type_dropdown'].setValue(this.types);
              this.createTaskForm.controls['user_participant'].setValue(this.setTaskUsers);
              //this.createTaskForm.controls['collaborators'].setValue([]);
              this.createTaskForm.controls['subtasks'].setValue([]);

              if (this.taskDetails.subtasks.length) {
                this.taskDetails.subtasks.forEach((value, index) => {
                  let subtask_user = [];
                  this.addSubtask();
                  this.getUser();
                  this.user_dropdown.forEach(function (val, key) {
                    if (val.id == value.assigned_to) {                     
                      subtask_user.push({
                        'id': val.id,
                        'user_email': val.user_email,
                        'user_name': val.user_name
                      });
                    }
                  });
                  let subtask_date;
                  if (value.date) { subtask_date = value.date.split("T");}
                  let createSubtask = (this.createTaskForm.controls['subtasks'] as FormArray).at(index) as FormGroup;
                  createSubtask.get('title').patchValue(value.title);
                  createSubtask.get('description').patchValue(value.description);
                  createSubtask.get('date').patchValue(subtask_date[0]);
                  createSubtask.get('assigned_to').patchValue(subtask_user);
                  // ((this.createTaskForm.get('subtasks') as FormArray).at(index) as FormGroup).get('title').patchValue(value.title);
                });
              }
            }
          }
        );
    }
  }

}
