import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';
import { Location } from '@angular/common';
import { LanguageService } from '../../../service/language.service';
declare var $: any;

@Component({
	selector: 'app-task-detail',
	templateUrl: './task-detail.component.html',
	styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {
	language;
	taskDetails: any = null;
	imageurl: any;
	collaboratorDetails: any = [];

	constructor(
		private authService: AuthServiceService,
		private router: Router,
		private route: ActivatedRoute,
		private _location: Location,
		private confirmDialogService: ConfirmDialogService,
		private lang: LanguageService
	) { }

	ngOnInit(): void {
		this.language = this.lang.getLanguaageFile();
		this.route.params.subscribe(params => {
			const taskid = params['taskid'];
			this.getTaskDetails(taskid);
		});
	}

	goBack() {
		this._location.back();
	}

	getTaskDetails(taskid) {
		if (sessionStorage.getItem('token')) {
			this.authService.setLoader(true);
			var user_id = localStorage.getItem('user-id');
			this.authService.memberSendRequest('get', 'get-task-by-id/' + taskid, null)
				.subscribe(
					(respData: JSON) => {
						this.authService.setLoader(false);
						console.log(respData['result'][0]);
						this.taskDetails = respData['result'][0];
						if (this.taskDetails) {
							this.getOrganizerDetails(taskid);
						}
					}
				);
		}
	}

	getOrganizerDetails(taskid) {
		if (sessionStorage.getItem('token')) {
			this.authService.setLoader(true);
			var user_id = localStorage.getItem('user-id');
			this.authService.memberSendRequest('get', 'getTaskCollaborator/task/' + taskid, null)
				.subscribe(
					(respData: JSON) => {
						this.authService.setLoader(false);
						console.log("Organizer Details");
						console.log(respData);
						for (const key in respData) {
							if (Object.prototype.hasOwnProperty.call(respData, key)) {
								const element = respData[key];
								this.collaboratorDetails.push(element);
							}
						}
					}
				);
		}
	}

	showToggle: boolean = false;
	onShow() {
		let el = document.getElementsByClassName("bunch_drop");
		if (!this.showToggle) {
			this.showToggle = true;
			el[0].className = "bunch_drop show";
		}
		else {
			this.showToggle = false;
			el[0].className = "bunch_drop";
		}
	}

	deleteEvents(eventId) {
		let self = this;
		this.confirmDialogService.confirmThis(this.language.confirmation_message.delete_task, function () {
			self.authService.setLoader(true);
			self.authService.memberSendRequest('delete', 'event/' + eventId, null)
				.subscribe(
					(respData: JSON) => {
						self.authService.setLoader(false);
						const url: string[] = ["/organizer"];
						self.router.navigate(url);
					}
				)
		}, function () {
			console.log("No clicked");
			$('.dropdown-toggle').trigger('click');
		})
	}

	eventMarkComplete(subtaskId){
	    let self = this;
	    this.confirmDialogService.confirmThis(this.language.confirmation_message.complete_task, function () {  
	        self.authService.memberSendRequest('get', 'complete-subtask-by-id/'+subtaskId, null)
	        .subscribe(
	          (respData: JSON) => {
	            self.ngOnInit();
	          }
	        )
	      }, function () {  
	        $('#styled-checkbox-'+subtaskId).prop('checked', false);
	    })
	 }

}
