import { Component, OnInit, VERSION, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css'],
  providers: [DatePipe]
})
export class ProfileEditComponent implements OnInit {
  language;
  registrationForm: FormGroup;
  userData;
  role;
  user;
  datePipeString: string;
  formError: any = [];
  submitted = false;
  responseMessage = null;

  constructor(
    private authService: AuthServiceService,
    public formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private _router: Router,
    private lang : LanguageService
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.getProfileData();

    this.registrationForm = this.formBuilder.group({
      street: ['', Validators.required],
      street2: ['', Validators.required],
      city: ['', Validators.required],
      countryCode: [''],
      postCode: ['', Validators.required],
      poboxCity: [''],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      share: [''],
      birthDate: ['', Validators.required]
    });


  }

  getProfileData() {
    if (sessionStorage.getItem('token')) {
      let userId = localStorage.getItem('user-id');
      let userDetail = JSON.parse(localStorage.getItem('user-data'));
      /* this.authService.memberSendRequest('get', 'singleUserdetails/' + userId, userId) */
      this.authService.memberSendRequest('get', 'member-info/'+userDetail.database_id+'/'+userDetail.team_id+'/'+userDetail.member_id, userDetail)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            if (Object.keys(respData).length) {
              this.userData = respData;
              console.log(this.userData);
              this.role = userDetail.roles[0];
              this.setValue();
            }
          }
        );
    }
  }

  cancel() {
    this._router.navigate(["/profile"]);
  }

  print(){
    window.print();
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  setValue() {
    if (this.userData.birthDate) {
      /* this.datePipeString = this.datePipe.transform(this.user.birthday, 'yyyy/MM/dd');
        street2: [this.userData.street2, [Validators.required, Validators.pattern("^(?![s-])[a-zA-Z0-9]+$")]], 
        phone: [this.userData.phone, [Validators.required, Validators.pattern("^[/+]?[1-9]{1}?[0-9]{9}$")]],
        street: [this.userData.street, [Validators.required, Validators.pattern("^[a-zA-Z0-9]+( [a-zA-Z0-9]+)*$")]],
        street2: [this.userData.street2, [Validators.required, Validators.pattern("^[a-zA-Z0-9]+( [a-zA-Z0-9]+)*$")]],
        city: [this.userData.city, [Validators.required, Validators.pattern("^[a-zA-Z]+( [a-zA-Z]+)*$")]],
      */
      this.datePipeString = this.datePipe.transform(this.userData.birthDate, 'yyyy-MM-dd');
    }

    this.registrationForm = this.formBuilder.group({
      street: [this.userData.street, [Validators.required, this.noWhitespace]],
      street2: [this.userData.street2, [Validators.required, this.noWhitespace]],
      city: [this.userData.city, [Validators.required, this.noWhitespace]],
      countryCode: [this.userData.countryCode],
      postCode: [this.userData.postCode, [Validators.required, Validators.pattern("^[1-9]{1}?[0-9]*$")]],
      poboxCity: [this.userData.poboxCity],
      email: [this.userData.email, [Validators.required, Validators.email]],
      phone: [this.userData.phone, [Validators.required]],
      birthDate: [this.datePipeString, Validators.required]
    });
  }

  noWhitespace(control: FormControl) {
    console.log('Whitespace->',control.value);
    console.log((control.value || '').trim().length);
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }

  fileAttr = 'Choose File';
  name = 'Angular ' + VERSION.major;
  dataimage: any;
  @ViewChild('fileInput') fileInput: ElementRef;

  uploadFileEvt1() {
    console.log("img File 1");
  }

  registrationProcess() {
    this.submitted = true;
    console.log("Form Valid");
    console.log(this.registrationForm);
    console.log("Inside the Updated user");
    this.formError = [];
    console.log(this.registrationForm.value);

    if ((sessionStorage.getItem('token')) && (this.registrationForm.valid)) {
      let userData = JSON.parse(localStorage.getItem('user-data'));
      this.authService.setLoader(true);
      this.authService.memberSendRequest('post', 'modify-user-data/'+userData.database_id+'/'+userData.team_id+'/'+userData.member_id, this.registrationForm.value)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            console.log("EDIT profile");
            console.log(respData);
            this.successMessage(respData);
            this.submitted = false;
            if (respData['code'] == 400) {
              this.responseMessage = respData['message'];
            }
          }
        );
    }
  }

  successMessage(msg: any) {
    if (msg == "OK") {
      this.responseMessage = this.language.profile_bank.success_msg;
      this.registrationForm.reset();
      setTimeout(function () {
        this._router.navigate(["/profile"]);
      }, 3000);
      return true;
    }    
  }

}
