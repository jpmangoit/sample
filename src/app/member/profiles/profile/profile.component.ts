import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';

import { DomSanitizer } from '@angular/platform-browser';

declare var $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  language;
  responseMessage;
  submitted = false;
  c_password = false;
  userData;
  userDetails;
  thumbnail = null;
  role = '';
  user;
  changePasswordForm: FormGroup;

  constructor(
    private authService: AuthServiceService,
    public formBuilder: FormBuilder,
    private _router: Router,
    private lang: LanguageService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.getProfileData();

    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', Validators.compose([Validators.minLength(5), Validators.required, Validators.pattern("^[a-zA-Z0-9/!/#/$/%/^/&/*/(/)/_/-/+/=/@]*$")])],
      newPassword: ['', Validators.compose([Validators.minLength(5), Validators.required, Validators.pattern("^[a-zA-Z0-9/!/#/$/%/^/&/*/(/)/_/-/+/=/@]*$")])],
      confirmPassword: ['', Validators.compose([Validators.minLength(5), Validators.required, Validators.pattern("^[a-zA-Z0-9/!/#/$/%/^/&/*/(/)/_/-/+/=/@]*$")])],
    });
  }

  getProfileData() {
    if (sessionStorage.getItem('token')) {
      let userId = localStorage.getItem('user-id');
      console.log("getProfileData " + userId);
      let userData = JSON.parse(localStorage.getItem('user-data'));
      this.authService.setLoader(true);
      // this.authService.memberSendRequest('get', 'singleUserdetails/' + userId, userId)
      // this.authService.memberSendRequest('get', 'member-info?database_id='+userData.database_id+'&club_id='+userData.team_id+'&member_id='+userData.member_id, userData)
      this.authService.memberSendRequest('get', 'member-info/' + userData.database_id + '/' + userData.team_id + '/' + userData.member_id, userData)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            console.log("profile NEW API");
            console.log(respData);
            this.userDetails = respData;

            if('memberPhoto' in this.userDetails){
              console.log("respData memberPhoto");
              console.log(this.userDetails.memberPhoto);
              let objectURL = 'data:'+this.userDetails.memberPhoto.mimeType+';base64,' + this.userDetails.memberPhoto.imageData;
              this.thumbnail = this.sanitizer.bypassSecurityTrustUrl(objectURL);
            }

            this.role = userData.roles[0];
            // console.log("profile component");
            // console.log(respData[0]);
            /* if (Object.keys(respData).length)
              this.getUserDetails(respData[0]); */
          }
        );
    }
  }

  getUserDetails(data) {
    let user = data;
    let userData = data['users'][0];
    this.user = user;
    return this.userData = userData;
  }

  inEdit() {
    this._router.navigate(["/edit-profile"]);
  }

  print() {
    window.print();
  }

  checkPassword() {
    console.log("Check Password");
    var password = this.changePasswordForm.get('newPassword').value;
    var confirm_password = this.changePasswordForm.get('confirmPassword').value;
    if (password == confirm_password) {
      this.c_password = false;
    }
    else {
      this.c_password = true;
    }
  }

  closeModal() {
    console.log("CLOSE");
    this.changePasswordForm.reset();
    this.c_password = false;
  }

  changePassword() {
    this.checkPassword();
    this.submitted = true;
    if ((sessionStorage.getItem('token')) && (this.changePasswordForm.valid) && (this.c_password == false)) {
      this.authService.setLoader(true);
      // this.changePasswordForm.removeControl('oldPassword');
      // this.changePasswordForm.removeControl('confirmPassword');
      this.authService.memberSendRequest('post', 'change-password', this.changePasswordForm.value)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            console.log("change Password submit");
            console.log(respData);
            if (respData['isError'] == false) {
              this.responseMessage = respData['result'];
              // this.changePasswordForm.reset();
              setTimeout(() => {
                $('.btn-close').trigger('click');
                sessionStorage.clear();
                this._router.navigate(["/login"]);
              }, 5000);
            }
            if (respData['code'] == 400) {
              this.responseMessage = respData['message'];
            }
          }
        );
    }
  }

}
