import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';

@Component({
  selector: 'app-profile-club',
  templateUrl: './profile-club.component.html',
  styleUrls: ['./profile-club.component.css']
})
export class ProfileClubComponent implements OnInit {
  language;
  clubData;
  user;
  constructor(
    private authService: AuthServiceService,
    private _router: Router,
    private lang : LanguageService
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.authService.setLoader(true);
    this.getClubData();
  }
  
  getClubData(){
    if (sessionStorage.getItem('token')) {
      let userData = JSON.parse(localStorage.getItem('user-data'));
      this.clubData = userData.Club;
      this.authService.setLoader(false);
    }
  }

}
