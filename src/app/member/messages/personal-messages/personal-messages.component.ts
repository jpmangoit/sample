import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';
import { appSetting } from '../../../app-settings';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';
declare var $: any;

@Component({
  selector: 'app-personal-messages',
  templateUrl: './personal-messages.component.html',
  styleUrls: ['./personal-messages.component.css']
})
export class PersonalMessagesComponent implements OnInit {

  searchSentenceForm: FormGroup;

  replyMsgForm: FormGroup;
  replyMsgFormError: any = [];
  replyMsgSubmitted = false;

  extensions;
  imageType;
  language;
  personalMessage;
  starredMessage;
  sentMessage;
  draftMessage;
  allMailMessage;
  trashMessage;
  responseMessage: string = null;
  personalInbox = true;
  personalStarred = false;
  personalSent = false;
  personalDrafts = false;
  personalAllMail = false;
  personalTrash = false;
  isReplyMsgForm = false;
  selectedMessage = [];
  alluserDetails:any = [];
  singleParticipent = false;
  multipleParticipent = false;
  userDetails;
  alluserInfo;
  visiblity = [];
  visiblityDropdownSettings;

  userDropdownList = [];
  userDropdownSettings;

  userDropdownCCList = [];
  userDropdownCCSettings;
  groups;
  groupDropdownSettings;

  receiverUser = [];
  ccUser = [];
  kindIds = [];

  messageForm: FormGroup;
  formError: any = [];
  messageSubmitted = false;

  selectedVisiblity;
  selectedKindId;


  personalVisiable= true;
  groupVisiable= false;
  clubVisiable= false;
  
  constructor(
    private lang : LanguageService,
    private authService: AuthServiceService,
    public formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService
  ) { }

  ngOnInit(): void {
    let self = this;
    this.language = this.lang.getLanguaageFile();
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));
    this.extensions = appSetting.extensions;
    this.imageType =  appSetting.imageType;
    this.getPersonalMessage();
    this.replyMsgForm = this.formBuilder.group({
      content: ['', Validators.required],
      add_image: ['']
    });

    this.searchSentenceForm = this.formBuilder.group({
      sentence: ['', Validators.required],
      currentUid: [2]
    });
    
    this.messageForm = new FormGroup({
      'kind': new FormControl('personal'),
      'receiver_id': new FormControl('', Validators.required),
      'subject': new FormControl('', Validators.required),
      'content': new FormControl('', Validators.required),
      'type': new FormControl('text'),
      'sender_id': new FormControl(this.userDetails.id),
      'file': new FormControl(''),
      'message_type': new FormControl('inbox'),
      'kind_id': new FormControl(''),
      'cc': new FormControl('')
    })

    this.authService.memberSendRequest('get', 'teamUsers/team/1', null)
    .subscribe(
      (respData: JSON) => {
        Object(respData).forEach((val,key) => {       
          this.alluserDetails[val.keycloak_id] = {firstname: val.firstname, lastname: val.lastname, email:val.email};
          this.userDropdownList.push({ 'id': val.keycloak_id, 'name': val.firstname + ' ' + val.lastname });
          this.userDropdownCCList.push({ 'id': val.keycloak_id, 'name': val.firstname + ' ' + val.lastname });
        })
        
        this.alluserInfo = respData;
          self.userDropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            enableCheckAll: false,
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };

          self.userDropdownCCSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            enableCheckAll: false,
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };
      })
  }

  searchSentence(){
    console.log("this.searchSentenceForm.value SEARCH");
    console.log(this.searchSentenceForm.get('sentence').value);
    console.log(this.searchSentenceForm.value);
    this.authService.setLoader(true);
    this.authService.memberSendRequest('post', 'message/search', this.searchSentenceForm.value)
        .subscribe(
          (respData) => {
            this.authService.setLoader(false);
            console.log("respData SEARCH response");
            console.log(respData);
            /* if (respData['isError'] == false) {
              this.responseMessage = respData['result']['message'];
              this.createNewsForm.reset();
              this.groupVisiblity = '';
              var url = 'assets/img/event_upload.png';
              $('.preview_img').attr('src', url);
              $('.preview_txt').hide();
              $('.preview_txt').text('');
            }
            if (respData['code'] == 400) {
              this.responseMessage = respData['message'];
            } */
          },
          (err) => {
            console.log("ERROR");
            console.log(err);
          }
        );
  }

  getPersonalMessage(){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);  
    this.authService.memberSendRequest('get', 'message/get-inbox', null)
      .subscribe(
        (respData: any) => {
          this.responseMessage = null;
          this.personalMessage = [];
          this.personalMessage = respData.reverse();
          this.personalInbox = true;
          this.personalStarred = false;
          this.personalSent = false;
          this.personalDrafts = false;
          this.personalAllMail = false;
          this.personalTrash = false;
          this.authService.setLoader(false);
          if (this.personalMessage.length > 0) {
            this.selectedMessage.push(this.personalMessage[0]);
          }
        }
      )
  }

  personalMessages(){   
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'message/get-inbox', null)
      .subscribe(
        (respData: any) => {
          this.responseMessage = null;
          this.personalMessage = [];
          this.personalMessage = respData.reverse();
          this.personalInbox = true;
          this.personalStarred = false;
          this.personalSent = false;
          this.personalDrafts = false;
          this.personalAllMail = false;
          this.personalTrash = false;
          this.authService.setLoader(false);
          if (this.personalMessage.length > 0) {
            this.selectedMessage.push(this.personalMessage[0])
          }
        }
      )
  }

  personalStarredMessages(){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'message/get-starred', null)
      .subscribe(
        (respData: any) => {
          this.responseMessage = null;
          this.personalMessage = [];
          this.personalMessage = respData.reverse();
          this.personalInbox = false;
          this.personalStarred = true;
          this.personalSent = false;
          this.personalDrafts = false;
          this.personalAllMail = false;
          this.personalTrash = false;
          this.authService.setLoader(false);
          if (this.personalMessage.length > 0) {
            this.selectedMessage.push(this.personalMessage[0])
          }
        }
      )
  }

  personalSentMessages(){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'message/get-sent', null)
      .subscribe(
        (respData: any) => {
          this.responseMessage = null;
          this.personalMessage = [];
          this.personalMessage = respData.reverse();
          this.personalInbox = false;
          this.personalStarred = false;
          this.personalSent = true;
          this.personalDrafts = false;
          this.personalAllMail = false;
          this.personalTrash = false;
          this.authService.setLoader(false);
          if (this.personalMessage.length > 0) {
            this.selectedMessage.push(this.personalMessage[0])
          }
        }
      )
  }

  personalDraftsMessages(){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'message/get-draft', null)
      .subscribe(
        (respData: any) => {
          this.responseMessage = null;
          this.personalMessage = [];
          this.personalMessage = respData.reverse();
          this.personalInbox = false;
          this.personalStarred = false;
          this.personalSent = false;
          this.personalDrafts = true;
          this.personalAllMail = false;
          this.personalTrash = false;
          this.authService.setLoader(false);
        }
      )
  }

  personalAllMailMessages(){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'message/get-all-mail', null)
      .subscribe(
        (respData: any) => {
          this.responseMessage = null;
          this.personalMessage = [];
          this.personalMessage = respData.reverse();
          this.personalInbox = false;
          this.personalStarred = false;
          this.personalSent = false;
          this.personalDrafts = false;
          this.personalAllMail = true;
          this.personalTrash = false;
          this.authService.setLoader(false);
          if (this.personalMessage.length > 0) {
            this.selectedMessage.push(this.personalMessage[0])
          }
        }
      )
  }

  personalTrashMessages(){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'message/get-trash', null)
      .subscribe(
        (respData: any) => {
          this.responseMessage = null;
          this.personalMessage = [];
          this.personalMessage = respData.reverse();
          this.personalInbox = false;
          this.personalStarred = false;
          this.personalSent = false;
          this.personalDrafts = false;
          this.personalAllMail = false;
          this.personalTrash = true;
          this.authService.setLoader(false);
          if (this.personalMessage.length > 0) {
            this.selectedMessage.push(this.personalMessage[0])
          }
        }
      )
  }

  markedStarredMessages(messageId,esdb_id){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    let msgMoveData = {
      "id": messageId,
      "esdb_id":esdb_id,
      "to": "starred"
    };
    this.authService.memberSendRequest('post', 'message/move', msgMoveData)
      .subscribe(
        (respData: any) => {
          this.authService.setLoader(false);
          this.responseMessage = 'Message move starreds successfully';
          let selectedTab = $('.feature_tab .active a').text().trim();
          setTimeout(() => {
            if(selectedTab == 'Inbox') {
              this.personalMessages();
            }else if(selectedTab == 'Starred') {
              this.personalStarredMessages();
            }else if(selectedTab == 'Sent') {
              this.personalSentMessages();
            }else if(selectedTab == 'Drafts') {
              this.personalDraftsMessages();
            }else if(selectedTab == 'All Mails') {
              this.personalAllMailMessages();
            }else if(selectedTab == 'Trash') {
              this.personalTrashMessages();
            }
          },500);
        }
      )
  }

  unmarkedStarredMessages(messageId,esdb_id){
    this.isReplyMsgForm = false;
    this.selectedMessage = [];
    this.authService.setLoader(true);
    let msgMoveData = {
      "id": messageId,
      "esdb_id":esdb_id,
      "to": "inbox"
    };
    this.authService.memberSendRequest('post', 'message/move', msgMoveData)
      .subscribe(
        (respData: any) => {
          this.authService.setLoader(false);
          this.responseMessage = 'Message move inbox successfully';
          let selectedTab = $('.feature_tab .active a').text().trim();
          setTimeout(() => {
            this.personalStarredMessages();
          },1000);
        }
      )
  }

  clickMessages(id, esdb_id){
    //console.log(this.alluserDetails)
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.replyMsgSubmitted = false;  
    $(".widget-app-content").removeClass( "highlight" );
    this.selectedMessage = [];
    this.personalMessage.forEach((val, index) => {
      if (val.id == id){
        this.selectedMessage.push(val)
        this.authService.setLoader(false);
      }
    });
    this.isReplyMsgForm = false;
    console.log(this.selectedMessage)
    $("#message-"+id).parent().addClass('highlight');

    if (this.selectedMessage){
      if (this.selectedMessage[0].is_read == 0){
        this.authService.memberSendRequest('get', 'message/read-message/'+id, null)
        .subscribe(
          (respData: any) => {
            setTimeout(() => {
              $("#envelope-"+id).removeClass( "fa-envelope-o" ).addClass( "fa-envelope-open-o" );
            },500);
          }
        )
      }
    }


    // this.authService.memberSendRequest('get', 'message/get-message-by-id/'+id, null)
    //   .subscribe(
    //     (respData: any) => {
    //       this.authService.setLoader(false);
    //       this.selectedMessage = respData.result;         
    //       $("#message-"+id).parent().addClass('highlight');
    //       if (this.selectedMessage){
    //         if (this.selectedMessage[0].is_read == 0){
    //           this.authService.memberSendRequest('get', 'message/read-message/'+id, null)
    //           .subscribe(
    //             (respData: any) => {
    //               setTimeout(() => {
    //                 $("#envelope-"+id).removeClass( "fa-envelope-o" ).addClass( "fa-envelope-open-o" );
    //               },500);
    //             }
    //           )
    //         }
    //       }
    //     }
    //   )

    // this.selectedMessage = [];
    // this.personalMessage.forEach((val, index) => {
    //   if (val.id == id){
    //     this.selectedMessage.push(val)
    //   }
    // });   
    
  }

  clickDraftMessages(id, esdb_id){ 
    this.isReplyMsgForm = false;
    this.visiblityDropdownSettings ={};
    this.language = this.lang.getLanguaageFile();
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));
    this.selectedMessage = [];
    this.authService.setLoader(true);
    this.replyMsgSubmitted = false;  
    $(".widget-app-content").removeClass( "highlight" );
    this.selectedMessage = [];
    this.personalMessage.forEach((val, index) => {
      if (val.id == id){
        this.selectedMessage.push(val)
        this.authService.setLoader(false);
      }
    });
    this.isReplyMsgForm = false;
    console.log(this.selectedMessage)
    $("#message-"+id).parent().addClass('highlight');   
    let toUsers = [];
    let ccUsers = [];
    if(this.selectedMessage[0].to.length > 0) {
      this.selectedMessage[0].to.forEach((val, index) => {
        if(val){
          toUsers.push({ 'id': val, 'name': this.alluserDetails[val].firstname + ' ' + this.alluserDetails[val].lastname });
        }       
      });
    }

    if(this.selectedMessage[0].cc.length > 0) {
      this.selectedMessage[0].cc.forEach((val, index) => {
        if(val){
          ccUsers.push({ 'id': val, 'name': this.alluserDetails[val].firstname + ' ' + this.alluserDetails[val].lastname });
        }
      });
    }    
    this.messageForm.controls["kind"].setValue('personal');
    this.messageForm.controls["subject"].setValue(this.selectedMessage[0].subject);
    this.messageForm.controls["content"].setValue(this.selectedMessage[0].content);
    this.messageForm.controls["receiver_id"].setValue(toUsers);
    this.messageForm.controls["cc"].setValue(ccUsers);
  }

  deleteMessages(messageId,esdb_id){
    this.isReplyMsgForm = false;
    let self = this;
    this.confirmDialogService.confirmThis(this.language.confirmation_message.send_msg_trash, function () {  
    self.selectedMessage = [];
    self.authService.setLoader(true);
    let msgMoveData = {
      "id": messageId,
      "esdb_id":esdb_id,
      "to": "trash"
    };
    self.authService.memberSendRequest('post', 'message/move', msgMoveData)
      .subscribe(
        (respData: any) => {
          self.authService.setLoader(false);
          self.responseMessage = 'Message move trash successfully';
          let selectedTab = $('.feature_tab .active a').text().trim();
          setTimeout(() => {
            if(selectedTab == 'Inbox') {
              self.personalMessages();
            }else if(selectedTab == 'Starred') {
              self.personalStarredMessages();
            }else if(selectedTab == 'Sent') {
              self.personalSentMessages();
            }else if(selectedTab == 'Drafts') {
              self.personalDraftsMessages();
            }else if(selectedTab == 'All Mails') {
              self.personalAllMailMessages();
            }
          },500);
        }
      )
    },function () { 
      console.log("No clicked");  
    })
  }

  deleteMessagesPermanently(messageId,esdb_id){
    this.isReplyMsgForm = false;
    let self = this;
    this.confirmDialogService.confirmThis(this.language.confirmation_message.permanently_delete_msg, function () {  
    self.authService.setLoader(true);
    self.authService.memberSendRequest('delete', 'message/deny-message/'+esdb_id, null)
      .subscribe(
        (respData: any) => {
          self.authService.setLoader(false);
          self.responseMessage = 'Message permanently delete successfully';
          let selectedTab = $('.feature_tab .active a').text().trim();
          setTimeout(() => {            
            self.personalTrashMessages();           
          },500);
        }
      )
    },function () { 
      console.log("No clicked");  
    })
  }

  stringifiedData(data){
    return JSON.parse(data)
  }

  replyToMessages(messageId,esdb_id){
    this.isReplyMsgForm = true;
    this.singleParticipent = true;
    this.multipleParticipent = false;
    setTimeout(() => {       
      $("#reply-heading").text("Reply");     
      $("#replyMsgType").val('reply');
      $("#replyToMsgId").val(esdb_id); 
    },500)
  }

  replayToAllMessages(messageId,esdb_id){
    this.isReplyMsgForm = true;
    this.singleParticipent = false;
    this.multipleParticipent = true;
    setTimeout(() => {
      $("#reply-heading").text("Reply to all");         
      $("#replyMsgType").val('replyAll');
      $("#replyToMsgId").val(esdb_id); 
    },500);
   
  }

  replyMessage(){
    let msgType = $("#replyMsgType").val();
    let esdb_id = $("#replyToMsgId").val();
    this.replyMsgSubmitted = true;
    if ((this.replyMsgForm.valid)) {
      var formData: any = new FormData();
      formData.append("file", this.replyMsgForm.get('add_image').value);
      formData.append("content", this.replyMsgForm.get('content').value)
      if(msgType == 'reply') {
        this.authService.setLoader(true);        
        this.authService.memberSendRequest('post', 'message/reply/'+esdb_id, formData)
          .subscribe(
            (respData: any) => {
              this.authService.setLoader(false);
              this.replyMsgSubmitted = false;              
              if(respData.isError ==  false){
                this.responseMessage = respData.result;
              }else{
                this.responseMessage = respData.result;
              }
              this.replyMsgForm.reset();
              this.isReplyMsgForm = false;
              let selectedTab = $('.feature_tab .active a').text().trim();
              setTimeout(() => {
                if(selectedTab == 'Inbox') {
                  this.personalMessages();
                }else if(selectedTab == 'Starred') {
                  this.personalStarredMessages();
                }else if(selectedTab == 'Sent') {
                  this.personalSentMessages();
                }else if(selectedTab == 'Drafts') {
                  this.personalDraftsMessages();
                }else if(selectedTab == 'All Mails') {
                  this.personalAllMailMessages();
                }
              },500);
            }
          )
      }else{
        this.authService.setLoader(true);
        this.authService.memberSendRequest('post', 'message/reply-to-all/'+esdb_id, formData)
          .subscribe(
            (respData: any) => {
              this.authService.setLoader(false);
              this.replyMsgSubmitted = false;
              if(respData.isError ==  false){
                this.responseMessage = respData.result;
              }else{
                this.responseMessage = respData.result;
              }
              this.replyMsgForm.reset();
              this.isReplyMsgForm = false;
              let selectedTab = $('.feature_tab .active a').text().trim();
              setTimeout(() => {
                if(selectedTab == 'Inbox') {
                  this.personalMessages();
                }else if(selectedTab == 'Starred') {
                  this.personalStarredMessages();
                }else if(selectedTab == 'Sent') {
                  this.personalSentMessages();
                }else if(selectedTab == 'Drafts') {
                  this.personalDraftsMessages();
                }else if(selectedTab == 'All Mails') {
                  this.personalAllMailMessages();
                }
              },500);
            }
          )
      } 
    }
    
  }

  errorImage: any = { isError: false, errorMessage: ''};
  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];    
    const mimeType = file.type;
      this.errorImage = { Error: true, errorMessage: ''};
      this.replyMsgForm.patchValue({
        add_image: file
      });
      this.replyMsgForm.get('add_image').updateValueAndValidity();

    const reader = new FileReader();
    var imagePath = file;
    reader.readAsDataURL(file);
    var url;
    reader.onload = (_event) => {
      url = reader.result;
      $('.message-upload-list').show();
      if (mimeType.match(/image\/*/)) {
        $('.preview_img').attr('src', url);
      }else{
        $('.preview_img').attr('src', 'assets/img/doc-icons/chat_doc_ic.png');
      }
      
    }
    $('.preview_txt').show();
    $('.preview_txt').text(file.name);
  }

  uploadDraftFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    const mimeType = file.type;
    this.messageForm.patchValue({
      file: file
    });
    this.messageForm.get('file').updateValueAndValidity();
    const reader = new FileReader();
    var imagePath = file;
    reader.readAsDataURL(file);
    var url;
    reader.onload = (_event) => {
      url = reader.result;
      $('.message-upload-list').show();
      if (mimeType.match(/image\/*/)) {
        $('.preview_img').attr('src', url);
      }else{
        $('.preview_img').attr('src', 'assets/img/doc-icons/chat_doc_ic.png');
      }
      
    }
    
    $('.preview_txt').show();
    $('.preview_txt').text(file.name);
  }

showToggle: boolean = false;
  onShow() {
    let el = document.getElementsByClassName("reply-users");
    if (!this.showToggle) {
      this.showToggle = true;
      el[0].className = "reply-users show";
    }
    else {
      this.showToggle = false;
      el[0].className = "reply-users";
    }
  }    
  showMore: boolean = false;
  onOpen() {
    let el = document.getElementsByClassName("multipl-participent-reply reply-users");
    if (!this.showMore) {
      this.showMore = true;
      el[0].className = "multipl-participent-reply reply-users show";
    }
    else {
      this.showMore = false;
      el[0].className = "multipl-participent-reply reply-users";
    }
  }

  messageProcess(){
    this.messageSubmitted = true;
    if (this.messageForm.valid) {      
      var formData: any = new FormData(); 
      this.receiverUser = [];
      this.ccUser = [];
      if (this.messageForm.controls["receiver_id"].value.length > 0){
        this.messageForm.controls["receiver_id"].value.forEach((val, index) => {
          this.receiverUser.push(val.id)
         });;
      }
      if (this.messageForm.controls["cc"].value.length > 0){
        this.messageForm.controls["cc"].value.forEach((val, index) => {
          this.ccUser.push(val.id)
         });;
      }
      this.messageForm.controls["receiver_id"].setValue(this.receiverUser);
      this.messageForm.controls["cc"].setValue(this.ccUser);
      
      console.log('this.messageForm.value')
      console.log(this.messageForm.value)
      for (const key in this.messageForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.messageForm.value, key)) {
          const element = this.messageForm.value[key];
          if (key == 'file') {            
            formData.append('file', element);
          }else if (key == 'receiver_id') {
            element.forEach(function (value, key) {
              formData.append("receiver_id[" + key + "]", value);            
            });
          }else if (key == 'cc') {
            element.forEach(function (value, key) {
              formData.append("cc[" + key + "]", value);            
            });
          }else{
            formData.append(key, element);
          }
        }
      };

      this.authService.memberSendRequest('post', 'message/send', formData)
      .subscribe(
        (respData) => {
          this.messageSubmitted = false;
          if(respData['isError'] == false){
            this.responseMessage = respData['result'];
            this.messageForm.reset();
            this.messageForm.controls["kind"].setValue([]);
            this.messageForm.controls["receiver_id"].setValue([]);
            this.messageForm.controls["cc"].setValue([]);
            this.authService.memberSendRequest('delete', 'message/delete-draft/'+this.selectedMessage[0].id, null)
            .subscribe(
              (respData: any) => {
                this.authService.setLoader(false);
                this.responseMessage = 'Message sent successfully';
                setTimeout(() => {
                  this.personalDraftsMessages();
                },500);
              }
            )
          }
          if(respData['code'] == 400){
            this.responseMessage = respData['message'];
          }
        },
        (err) => {
          console.log("ERROR");
          console.log(err);
        }
      );
      
    }
  }

  deleteDraftMessages(messageId,esdb_id){
    this.isReplyMsgForm = false;
    let self = this;
    this.confirmDialogService.confirmThis(this.language.confirmation_message.permanently_delete_msg, function () {  
    self.selectedMessage = [];
    self.authService.setLoader(true);
    self.authService.memberSendRequest('delete', 'message/delete-draft/'+messageId, null)
      .subscribe(
        (respData: any) => {
          self.authService.setLoader(false);
          self.responseMessage = 'Message permanently delete successfully';
          let selectedTab = $('.feature_tab .active a').text().trim();
          setTimeout(() => {
            self.personalDraftsMessages();
          },500);
        }
      )
    },function () { 
      console.log("No clicked");  
    })
  }

  onReceiverSelect(item: any) {
    this.receiverUser.push(item.id);
  }

  onReceiverDeSelect(item: any) {
    const index =  this.receiverUser.indexOf(item.id);
    if (index > -1) {
      this.receiverUser.splice(index, 1);
    }
  }

  onCCSelect(item: any) {
    this.ccUser.push(item.id);
  }

}
