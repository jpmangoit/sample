import { Component, OnInit } from '@angular/core';
import { appSetting } from '../../../app-settings';
import { LanguageService } from '../../../service/language.service';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from '../../../service/auth-service.service';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';
declare var $: any;
@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.component.html',
  styleUrls: ['./create-message.component.css']
})
export class CreateMessageComponent implements OnInit {
  language;
  userAccess;
  createAccess;
  participateAccess;
  authorizationAccess;

  visiblity = [];
  visiblityDropdownSettings;
  userDetails;

  alluserInformation = [];
  alluserDetails;
  userDropdownList = [];
  userDropdownSettings;

  userDropdownCCList = [];
  userDropdownCCSettings;
  groups;
  groupDropdownSettings;

  receiverUser = [];
  ccUser = [];
  kindIds = [];


  responseMessage = null;
  messageForm: FormGroup;
  formError: any = [];
  messageSubmitted = false;

  selectedVisiblity;
  selectedKindId;


  personalVisiable = true;
  groupVisiable = false;
  clubVisiable = false;

  files: string[] = [];
  receipientUsers = [];
  constructor(
    private lang: LanguageService,
    private authService: AuthServiceService,
    public formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));
    let userRole = this.userDetails.roles[0];
    this.userAccess = appSetting.role;
    this.createAccess = this.userAccess[userRole].create;
    this.participateAccess = this.userAccess[userRole].participate;
    this.authorizationAccess = this.userAccess[userRole].authorization;

    this.getGroup();
    this.getAllUserInfo();
    this.visiblity = [
      { "id": "personal", "name": this.language.create_message.personalMsg },
      { "id": "group", "name": this.language.create_message.groupMsg },
    ];
    if (this.createAccess.message == 'Yes') {
      this.visiblity.push({ "id": "club", "name": this.language.create_message.clubMsg })
    }

    this.visiblityDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      enableCheckAll: false,
      unSelectAllText: 'UnSelect All'
    };

    this.messageForm = new FormGroup({
      'kind': new FormControl('', Validators.required),
      'receiver_id': new FormControl('', Validators.required),
      'subject': new FormControl('', Validators.required),
      'content': new FormControl('', Validators.required),
      'type': new FormControl('text'),
      'sender_id': new FormControl(this.userDetails.id),
      'file': new FormControl(''),
      'message_type': new FormControl('inbox'),
      'kind_id': new FormControl(''),
      'cc': new FormControl('')
    });
  }

  close() {
    let self = this;
    
    console.log("Message Form");
    console.log(this.kindIds);
    console.log(this.messageForm);

    if (this.receiverUser.length > 0 || this.kindIds.length > 0 || this.kindIds.length > 0) {
      this.confirmDialogService.confirmThis(this.language.confirmation_message.save_msg_draft, function () {
        var formData: any = new FormData();
        self.messageForm.controls["kind"].setValue(self.selectedVisiblity);
        self.messageForm.controls["receiver_id"].setValue(self.receiverUser);
        self.messageForm.controls["cc"].setValue(self.ccUser);
        self.messageForm.controls["kind_id"].setValue(self.selectedKindId);
        self.messageForm.controls["message_type"].setValue('draft');
        for (const key in self.messageForm.value) {
          if (Object.prototype.hasOwnProperty.call(self.messageForm.value, key)) {
            const element = self.messageForm.value[key];
            if (key == 'file') {
              formData.append('file', element);
            } else if (key == 'receiver_id') {
              element.forEach(function (value, key) {
                formData.append("receiver_id[" + key + "]", value);
              });
            } else if (key == 'cc') {
              element.forEach(function (value, key) {
                formData.append("cc[" + key + "]", value);
              });
            } else {
              formData.append(key, element);
            }
          }
        };

        console.log("DATA FINAL");
        console.log(formData.get('kind'));
        console.log(formData.get('kind_id'));
        console.log(formData.getAll('cc'));
        console.log(formData.getAll('receiver_id'));

        let kindValue = self.messageForm.controls["kind"].value;
        if (kindValue == 'personal') {
          self.authService.memberSendRequest('post', 'message/send', formData)
            .subscribe(
              (respData) => {
                self.messageSubmitted = false;
                if (respData['isError'] == false) {
                  self.responseMessage = 'Email save as draft';
                  self.messageForm.reset();
                  self.messageForm.controls["kind"].setValue([]);
                  self.messageForm.controls["receiver_id"].setValue([]);
                  self.messageForm.controls["cc"].setValue([]);
                  $(".message_title").click();
                  setTimeout(() => {
                    localStorage.setItem('backItem', 'personalMsg');
                    const url: string[] = ["/community"];
                    self.router.navigate(url);
                  }, 1000);
                }
                if (respData['code'] == 400) {
                  self.responseMessage = respData['message'];
                }
              },
              (err) => {
                console.log("ERROR");
                console.log(err);
              }
            );
        } else if (kindValue == 'group') {
          self.authService.memberSendRequest('post', 'message/send-group-message', formData)
            .subscribe(
              (respData) => {
                self.messageSubmitted = false;
                if (respData['isError'] == false) {
                  self.responseMessage = 'Email save as draft';
                  self.messageForm.controls["kind"].setValue([]);
                  self.messageForm.controls["kind_id"].setValue([]);
                  self.messageForm.reset();
                  $(".message_title").click();
                  setTimeout(() => {
                    localStorage.setItem('backItem', 'groupMsg');
                    const url: string[] = ["/community"];
                    self.router.navigate(url);
                  }, 1000);
                }
                if (respData['code'] == 400) {
                  self.responseMessage = respData['message'];
                }
              },
              (err) => {
                console.log("ERROR");
                console.log(err);
              }
            );
        } else if (kindValue == 'club') {
          self.authService.memberSendRequest('post', 'message/send-club-message', formData)
            .subscribe(
              (respData) => {
                self.messageSubmitted = false;
                if (respData['isError'] == false) {
                  self.responseMessage = 'Email save as draft';
                  self.messageForm.controls["kind"].setValue([]);
                  self.messageForm.controls["receiver_id"].setValue([]);
                  self.messageForm.controls["cc"].setValue([]);
                  self.messageForm.reset();
                  $(".message_title").click();
                  setTimeout(() => {
                    localStorage.setItem('backItem', 'clubMsg');
                    const url: string[] = ["/community"];
                    self.router.navigate(url);
                  }, 1000);
                }
                if (respData['code'] == 400) {
                  self.responseMessage = respData['message'];
                }
              },
              (err) => {
                console.log("ERROR");
                console.log(err);
              }
            );
        }
      }, function () {
        self.router.navigate(['community']);
      });
    } else {
      self.router.navigate(['community']);
    }
  }

  getAllUserInfo() {
    let self = this;
    this.authService.memberSendRequest('get', 'teamUsers/team/1', null)
      .subscribe(
        (respData: JSON) => {
          Object(respData).forEach((val, key) => {
            this.alluserInformation[val.keycloak_id] = { firstname: val.firstname, lastname: val.lastname, email: val.email };
            this.userDropdownList.push({ 'id': val.keycloak_id, 'name': val.firstname + ' ' + val.lastname });
            this.userDropdownCCList.push({ 'id': val.keycloak_id, 'name': val.firstname + ' ' + val.lastname });
          })
          this.alluserDetails = respData;
          self.userDropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            enableCheckAll: true,
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };
          self.userDropdownCCSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            enableCheckAll: false,
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };
        }
      );
  }

  getGroup() {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'teamgroups/1', null)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            this.groups = respData;
            this.groupDropdownSettings = {
              singleSelection: true,
              idField: 'id',
              textField: 'name',
              selectAllText: 'Select All',
              enableCheckAll: false,
              unSelectAllText: 'UnSelect All',
              allowSearchFilter: true
            };
          }
        );
    }
  }

  messageProcess() {
    this.messageSubmitted = true;
    console.log(this.receiverUser);
    console.log("this.receiverUser");
    console.log(this.messageForm);
    console.log(this.selectedKindId);
    if (this.messageForm.valid) {
      var formData: any = new FormData();
      this.messageForm.controls["kind"].setValue(this.selectedVisiblity);
      this.messageForm.controls["receiver_id"].setValue(this.receiverUser);
      this.messageForm.controls["cc"].setValue(this.ccUser);
      this.messageForm.controls["kind_id"].setValue(this.selectedKindId);
      for (const key in this.messageForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.messageForm.value, key)) {
          const element = this.messageForm.value[key];
          //console.log('key----'+key)
          if (key == 'file') {
            formData.append('file', element);
            // for  (var i =  0; i <  this.files.length; i++)  {  
            //   formData.append('file',  this.files[i]);
            // } 
          } else if (key == 'receiver_id') {
            element.forEach(function (value, key) {
              formData.append("receiver_id[" + key + "]", value);
            });
          } else if (key == 'cc') {
            element.forEach(function (value, key) {
              formData.append("cc[" + key + "]", value);
            });
          } else {
            formData.append(key, element);
          }
        }
      };
      let kindValue = this.messageForm.controls["kind"].value;
      if (kindValue == 'personal') {
        this.authService.memberSendRequest('post', 'message/send', formData)
          .subscribe(
            (respData) => {
              this.messageSubmitted = false;
              if (respData['isError'] == false) {
                this.receipientUsers = [];
                this.receiverUser = [];
                this.ccUser = [];
                this.responseMessage = respData['result'];
                this.messageForm.reset();
                this.messageForm.controls["kind"].setValue([]);
                this.messageForm.controls["receiver_id"].setValue([]);
                this.messageForm.controls["cc"].setValue([]);
                $(".message_title").click();
                setTimeout(() => {
                  localStorage.setItem('backItem', 'personalMsg');
                  const url: string[] = ["/community"];
                  this.router.navigate(url);
                }, 1000);
              }
              if (respData['code'] == 400) {
                this.responseMessage = respData['message'];
              }
            },
            (err) => {
              console.log("ERROR");
              console.log(err);
            }
          );
      } else if (kindValue == 'group') {
        this.authService.memberSendRequest('post', 'message/send-group-message', formData)
          .subscribe(
            (respData) => {
              this.messageSubmitted = false;
              if (respData['isError'] == false) {
                this.receipientUsers = [];
                this.receiverUser = [];
                this.ccUser = [];
                this.responseMessage = respData['result'];
                this.messageForm.controls["kind"].setValue([]);
                this.messageForm.controls["kind_id"].setValue([]);
                this.messageForm.reset();
                $(".message_title").click();
                setTimeout(() => {
                  localStorage.setItem('backItem', 'groupMsg');
                  const url: string[] = ["/community"];
                  this.router.navigate(url);
                }, 1000);
              }
              if (respData['code'] == 400) {
                this.responseMessage = respData['message'];
              }
            },
            (err) => {
              console.log("ERROR");
              console.log(err);
            }
          );
      } else if (kindValue == 'club') {
        this.authService.memberSendRequest('post', 'message/send-club-message', formData)
          .subscribe(
            (respData) => {
              this.messageSubmitted = false;
              if (respData['isError'] == false) {
                this.receipientUsers = [];
                this.receiverUser = [];
                this.ccUser = [];
                this.responseMessage = respData['result'];
                this.messageForm.controls["kind"].setValue([]);
                this.messageForm.controls["receiver_id"].setValue([]);
                this.messageForm.controls["cc"].setValue([]);
                this.messageForm.reset();
                $(".message_title").click();
                setTimeout(() => {
                  localStorage.setItem('backItem', 'clubMsg');
                  const url: string[] = ["/community"];
                  this.router.navigate(url);
                }, 1000);
              }
              if (respData['code'] == 400) {
                this.responseMessage = respData['message'];
              }
            },
            (err) => {
              console.log("ERROR");
              console.log(err);
            }
          );
      }

    }
  }

  onVisiblityDeSelect(item: any) {
    this.receipientUsers = [];
    this.receiverUser = [];
    this.ccUser = [];
    this.messageForm.controls["receiver_id"].setValue('');
    this.messageForm.controls["cc"].setValue('');
    this.messageForm.controls["kind_id"].setValue('');
  }

  onVisiblitySelect(item: any) {
    this.selectedVisiblity = item.id;
    this.receipientUsers = [];
    this.receiverUser = [];
    this.ccUser = [];
    if (this.selectedVisiblity == "personal") {
      this.messageForm.controls["kind_id"].clearValidators();
      this.messageForm.controls["receiver_id"].setValidators(Validators.required);
      this.messageForm.controls["receiver_id"].setValue('');
      this.messageForm.controls["cc"].setValue('');
      this.messageForm.controls["receiver_id"].updateValueAndValidity();
      this.personalVisiable = true;
      this.groupVisiable = false;
      this.clubVisiable = false;
    }
    else if (this.selectedVisiblity == "group") {
      this.messageForm.controls["receiver_id"].clearValidators();
      this.messageForm.controls["receiver_id"].setValue('');
      this.messageForm.controls["cc"].setValue('');
      this.messageForm.controls["kind_id"].setValidators(Validators.required);
      this.messageForm.controls["kind_id"].setValue('');
      this.messageForm.controls["kind_id"].updateValueAndValidity();
      this.personalVisiable = false;
      this.groupVisiable = true;
      this.clubVisiable = false;
    } else {
      this.messageForm.controls["kind_id"].clearValidators();
      this.messageForm.controls["receiver_id"].setValidators(Validators.required);
      this.messageForm.controls["receiver_id"].setValue('');
      this.messageForm.controls["cc"].setValue('');
      this.messageForm.controls["receiver_id"].updateValueAndValidity();
      this.personalVisiable = true;
      this.groupVisiable = false;
      this.clubVisiable = true;
    }
  }

  onKindIdSelect(item: any) {
    this.kindIds = [];
    this.selectedKindId = item.id;
    this.kindIds.push(this.selectedKindId);
    this.authService.memberSendRequest('get', 'approvedGroupUsers/group/' + this.selectedKindId, null)
      .subscribe(
        (respData: any) => {
          this.receipientUsers = respData[0].participants;
          //console.log('this.receipientUsers')    
          //console.log(this.receipientUsers)   
        }
      )
  }

  onKindIdDeSelect(item: any) {
    this.selectedKindId = item.id;
    this.receipientUsers = [];
    const index = this.kindIds.indexOf(this.selectedKindId);
    if (index > -1) {
      this.kindIds.splice(index, 1);
    }
  }

  onReceiverSelect(item: any) {
    this.receiverUser.push(item.id);
  }

  onReceiverSelectAll(item: any) {
    console.log("SELECT ALL--");    
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.receiverUser.push(element.id);
      }
    }
  }

  onReceiverDeSelectAll(item: any) {
    console.log("DeSELECT ALL--");
    this.receiverUser = [];
  }

  onReceiverDeSelect(item: any) {
    const index = this.receiverUser.indexOf(item.id);
    if (index > -1) {
      this.receiverUser.splice(index, 1);
    }
  }

  onCCSelect(item: any) {
    this.ccUser.push(item.id);
  }

  onCCDeSelect(item: any) {
    const index = this.ccUser.indexOf(item.id);
    if (index > -1) {
      this.ccUser.splice(index, 1);
    }
  }


  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    const mimeType = file.type;
    this.messageForm.patchValue({
      file: file
    });
    this.messageForm.get('file').updateValueAndValidity();

    const reader = new FileReader();
    var imagePath = file;
    reader.readAsDataURL(file);
    var url;
    reader.onload = (_event) => {
      url = reader.result;
      if (mimeType.match(/image\/*/)) {
        $('.preview_img').attr('src', url);
      } else {
        $('.preview_img').attr('src', 'assets/img/doc-icons/chat_doc_ic.png');
      }

    }
    $('.message-upload-list').show();
    $('.preview_txt').show();
    $('.preview_txt').text(file.name);

  }
  onFileChange(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.files.push(event.target.files[i]);
    }
  }


}
