import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from '../../../service/auth-service.service';
import { LanguageService } from '../../../service/language.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-community-messages',
  templateUrl: './community-messages.component.html',
  styleUrls: ['./community-messages.component.css']
})
export class CommunityMessagesComponent implements OnInit {
  language;
  
  userDetails;
  chatHistory;
  selectedChat;
  chatHistoryForm: FormGroup;

  displayChats:boolean = false;
  displayPersonal:boolean = false;
  displayClub:boolean = false;
  displayGroup:boolean = false;

  constructor(
    private lang : LanguageService,
    private authService: AuthServiceService,
    public formBuilder: FormBuilder,
    private router:Router
  ) { 
    var getParamFromUrl = this.router.url.split("/")['2'];
    if(getParamFromUrl == 'personal-msg'){
      this.displayPersonal = true;
    }else if(getParamFromUrl == 'club-msg'){
      this.displayClub = true;
    }else if(getParamFromUrl == 'group-msg'){
    this.displayGroup = true;
    }else{
      this.displayChats = true;
    }
  }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));
    this.getChatHistory(this.userDetails.id);
    if (localStorage.getItem('backItem')){
      if(localStorage.getItem('backItem') == 'personalMsg'){
        localStorage.removeItem('backItem');
        this.displayMessage('personal');
      }else if(localStorage.getItem('backItem') == 'clubMsg'){
        localStorage.removeItem('backItem');
        this.displayMessage('club');
      }else if(localStorage.getItem('backItem') == 'groupMsg'){
        localStorage.removeItem('backItem');
        this.displayMessage('group');
      }
    }
  }

  getChatHistory(id){
    this.chatHistoryForm = this.formBuilder.group({
      currentUid: [id]
    });
    this.authService.setLoader(true);
    this.authService.memberSendRequest('post', 'get-chat-list', this.chatHistoryForm.value)
        .subscribe(
          (respData) => {
            this.authService.setLoader(false);
            console.log("respData chat history response");
            console.log(respData);
            this.chatHistory = respData;
          },
          (err) => {
            console.log("ERROR");
            console.log(err);
          }
        );
  }

  clickChat(chatId){
    // this.selectedChat = [];
    for (const key in this.chatHistory) {
      if (Object.prototype.hasOwnProperty.call(this.chatHistory, key)) {
        const element = this.chatHistory[key];
        if(element.id == chatId){
          // this.selectedChat.push(element);
          this.selectedChat = element;
        }
      }
    }
    console.log(this.selectedChat);
    // this.authService.setLoader(true);
    $(".widget-app-content").removeClass("highlight");
    $("#chat-"+chatId).addClass("highlight");
    /* if(!this.selectedChat.lastMessage.read){
      this.authService.memberSendRequest('get', 'message/read-message/', null)
        .subscribe(
          (respData: any) => {
            console.log("respData for READ--");
            console.log(respData);
            // setTimeout(() => {
            //   $("#envelope-"+id).removeClass( "fa-envelope-o" ).addClass( "fa-envelope-open-o" );
            // },500);
          }
        )
    } */
    
    
    /* this.authService.setLoader(true);
    this.replyMsgSubmitted = false;  
    $(".widget-app-content").removeClass( "highlight" );
    this.selectedMessage = [];
    this.personalMessage.forEach((val, index) => {
      if (val.id == id){
        this.selectedMessage.push(val)
        this.authService.setLoader(false);
      }
    });
    this.isReplyMsgForm = false;
    console.log(this.selectedMessage)
    $("#message-"+id).parent().addClass('highlight');

    if (this.selectedMessage){
      if (this.selectedMessage[0].is_read == 0){
        this.authService.memberSendRequest('get', 'message/read-message/'+id, null)
        .subscribe(
          (respData: any) => {
            setTimeout(() => {
              $("#envelope-"+id).removeClass( "fa-envelope-o" ).addClass( "fa-envelope-open-o" );
            },500);
          }
        )
      }
    } */
  }

  displayMessage(msg){
    if (msg == 'chat'){
      this.displayChats = true;
      this.displayPersonal = false;
      this.displayClub = false;
      this.displayGroup = false;
    }else if(msg == 'personal'){
      this.displayChats = false;
      this.displayPersonal = true;
      this.displayClub = false;
      this.displayGroup = false;
    }else if(msg == 'club'){
      this.displayChats = false;
      this.displayPersonal = false;
      this.displayClub = true;
      this.displayGroup = false;
    }else if(msg == 'group'){
      this.displayChats = false;
      this.displayPersonal = false;
      this.displayClub = false;
      this.displayGroup = true;
    }
  }

}
