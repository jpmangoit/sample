import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';
import { Location } from '@angular/common';
import { LanguageService } from '../../../service/language.service';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {
  language;
  groupData;
  groupJoinData;
  groupDetails = null;
  groupNewsDetails;
  groupAction = 0;
  showGroupNews;
  userId;
  userDetails

  currentPageNmuber: number = 1; 
  itemPerPage = 8;
  newsTotalRecords = 0;
  limitPerPage = [
    { value: '8'},
    { value: '16'},
    { value: '24'},
    { value: '32'},
    { value: '40'}
 ];

  constructor(
    private authService: AuthServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private confirmDialogService: ConfirmDialogService,
    private lang: LanguageService
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.userId = localStorage.getItem('user-id');
    this.userDetails = JSON.parse(localStorage.getItem('user-data'));
    this.route.params.subscribe(params => {
      const groupid = params['groupid'];
      this.getGroupDetails(groupid);
    });
  }

  getGroupDetails(groupid) {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      var user_id = localStorage.getItem('user-id');
      this.authService.memberSendRequest('get', 'approvedGroupUsers/group/' + groupid, null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            console.log(respData[0]);
            this.groupDetails = respData[0];
            for (const key in this.groupDetails.participants) {
              if (Object.prototype.hasOwnProperty.call(this.groupDetails.participants, key)) {
                const element = this.groupDetails.participants[key];
                console.log((element.user_id.toString()+"====="+user_id))
                if (element.user_id.toString() == user_id) {
                  this.groupAction = 1;
                  this.getGroupNews(groupid);
                }
                /* else{
                  this.groupAction = 0;
                } */
              }
            }
           
          }
        );
    }
  }

  getGroupNews(groupid) {
    if (sessionStorage.getItem('token')) {
      this.authService.setLoader(true);
      var user_id = localStorage.getItem('user-id');
      this.authService.memberSendRequest('get', 'groupNews/groupId/' + groupid, null)
        .subscribe(
          (respData: any) => {
            this.authService.setLoader(false);
            this.newsTotalRecords = respData.length;
            this.groupNewsDetails = respData;           
            this.showGroupNews = this.groupNewsDetails.length;
          }
        );
    }
  }

  showToggle: boolean = false;
  onShow() {
    let el = document.getElementsByClassName("bunch_drop");
    if (!this.showToggle) {
      this.showToggle = true;
      el[0].className = "bunch_drop show";
    }
    else {
      this.showToggle = false;
      el[0].className = "bunch_drop";
    }
  }

  goBack() {
    localStorage.setItem('backItem','groups');
    const url: string[] = ["/community"];
    this.router.navigate(url);
  }

  joinGroup(groupId) {
    let self = this;
    this.confirmDialogService.confirmThis(this.language.community_groups.join_group_popup, function () {
      let userId = localStorage.getItem('user-id');
      let postData = {
        "participants": {
          "group_id": groupId,
          "user_id": userId,
          "approved_status": 2
        }
      };
      self.authService.memberSendRequest('post', 'joinGroup/user_id/' + userId + '/group_id/' + groupId, postData)
        .subscribe(
          (respData: JSON) => {
            self.teamAllGroups();
            self.joinAllGroups();
          }
        )
    }, function () {
      console.log("No clicked");
    })
  }

  leaveGroup(groupId) {
    let self = this;
    this.confirmDialogService.confirmThis(this.language.community_groups.leave_group_popup, function () {
      let userId = localStorage.getItem('user-id');
      self.authService.memberSendRequest('delete', 'leaveGroup/user/' + userId + '/group_id/' + groupId, null)
        .subscribe(
          (respData: JSON) => {
            self.teamAllGroups();
            self.joinAllGroups();
          }
        )
    }, function () {
      console.log("No clicked");
    })
  }

  teamAllGroups() {
    let userId = localStorage.getItem('user-id');
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'getGroupsNotParticipant/user/' + userId, null)
      .subscribe(
        (respData: JSON) => {
          this.groupData = respData;
          this.authService.setLoader(false);
        }
      );
  }

  joinAllGroups() {
    let userId = localStorage.getItem('user-id');
    this.authService.setLoader(true);
    this.authService.memberSendRequest('get', 'approvedUserGroups/user/' + userId, null)
      .subscribe(
        (respData: JSON) => {
          this.groupJoinData = respData;
          this.authService.setLoader(false);
          this.router.navigate(['community']);
        }
      );
  }

  deleteGroup(groupId) {
    let self = this;
    this.confirmDialogService.confirmThis(this.language.community_groups.delete_group_popup, function () {
      self.authService.setLoader(true);
      self.authService.memberSendRequest('delete', 'deleteGroup/' + groupId, null)
        .subscribe(
          (respData: JSON) => {
            self.authService.setLoader(false);
            self.router.navigate(['community']);
          }
        )
    }, function () {
      console.log("No clicked");
    })
  }

  pageChanged(event){
    this.currentPageNmuber = event;
  }

  goToPg(eve: number) {
    if (isNaN(eve)) {
      eve = this.currentPageNmuber;
    }
    this.currentPageNmuber = eve;
  }
  setItemPerPage(limit: number) {
    if (isNaN(limit)) {
      limit = this.itemPerPage;
    }
    // if (limit > 50 || limit < 10) {
    //   limit = this.itemPerPage;
    // }
    this.itemPerPage = limit;
  }

}
