import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../../service/auth-service.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { LanguageService } from '../../../service/language.service';
import { CalendarOptions } from '@fullcalendar/angular';
declare var $: any;

@Component({
  selector: 'app-organizer-event',
  templateUrl: './organizer-event.component.html',
  styleUrls: ['./organizer-event.component.css'],
  providers: [DatePipe]
})
export class OrganizerEventComponent implements OnInit {
  language;
  currentEvent = [];
  currentEventList = [];
  upcomingEvent = [];
  upcomingEventList = [];
  pastEvent = [];
  todayEventToShow: any = [];
  upcomingEventToShow: any = [];
  upcomingEventLength: boolean = true;
  currentEventLength: boolean = true;
  allData = [];
  date;
  todays_date;
  name: string = "";
  auth_name: string = "";
  calendarData = [];
  calendarEvents = [];
  clickedEventData = [];
  calendarClicked = false;
  eventTypeList: any = [];
  newClickedEvent;
  calendarEventClicked = false;
  Events = [
    { 'title': "Event Title1", "start": "2021-09-09T12:30:00", "end": "2021-09-10T013:30:00", "description": "a test desc for event title1" },
    { 'title': "Event Title2", "start": "2021-08-28", "end": "2021-08-28", "description": "a test desc for event title2" },
    { 'title': "keys.name", 'start': "2021-08-28", 'end': "2021-08-28", 'description': "keys.description", 'event_id': "keys.id", 'type': "keys.type" }
  ];
  calendarOptions: CalendarOptions;
  calendarOptionsTimeGrid: CalendarOptions;

  constructor(
    private authService: AuthServiceService,
    private datePipe: DatePipe,
    private router: Router,
    private lang: LanguageService
  ) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.eventTypeList[1] = { name: this.language.create_event.club_event, class: "club-event-color" };
    this.eventTypeList[2] = { name: this.language.create_event.group_event, class: "group-event-color" };
    this.eventTypeList[3] = { name: this.language.create_event.functionaries_event, class: "functionaries-event-color" };
    this.eventTypeList[4] = { name: this.language.create_event.courses, class: "courses-event-color" };
    this.eventTypeList[5] = { name: this.language.create_event.seminar, class: "seminar-event-color" };

    this.calendarOptions = {
      initialView: 'dayGridMonth',
      dateClick: this.onDateClick.bind(this),
      eventColor: 'yellow',
      eventTextColor: 'black',
      eventDisplay: 'list-item',
      displayEventTime: false,
      displayEventEnd: true,
      defaultAllDay: false,
      forceEventDuration: true,
      events: this.calendarEvents
      // events: this.Events
    };
 
    this.calendarOptionsTimeGrid = {
      initialView: 'timeGridWeek',
      events: this.calendarEvents,
      eventClick:this.handleEventClick.bind(this),
      eventTextColor: 'black',
      eventDisplay: 'list-item',
      expandRows:false,  
      displayEventTime: false,
      displayEventEnd: true,
      defaultAllDay: false,
      forceEventDuration: true,
     // contentHeight:'auto',
      height: 1200 ,
      dateClick: this.onDateClick.bind(this),
      slotLabelFormat: [
        {
            hour: '2-digit',
            minute: '2-digit',
            hour12:false
        }
        ],
    };

    if (sessionStorage.getItem('token')) {
      let userId = localStorage.getItem('user-id');
      this.authService.setLoader(true);
      /* this.authService.memberSendRequest('get', 'events/', null) */
      this.authService.memberSendRequest('get', 'approvedEvents/user/' + userId, null)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            this.date = new Date(); // Today's date
            this.todays_date = this.datePipe.transform(this.date, 'yyyy-MM-dd');
            var element = null;
            var ccount = 0;
            var ucount = 0;
            var pcount = 0;
            for (var key in respData) {
              if (respData.hasOwnProperty(key)) {
                element = respData[key];
                var url = [];
                for (const key in element) {
                  if (Object.prototype.hasOwnProperty.call(element, key)) {
                    const value = element[key];
                    if (key == 'picture_video') {
                      url = value.split('\"');
                      // console.log(url.length);
                    }
                  }
                }
                if (url.length > 1) {
                  element['picture_video'] = url[1];
                }
                else {
                  element['picture_video'] = '';
                }
                this.allData[key] = element;
                /* element.forEach(function(value, key) {
                  if(key=='picture_video'){
                    //console.log("picture_video "+value);
                    console.log(value.split("["));
                  }
                }); */
                // console.log("approvedEventUser=s=");
                // console.log(element.date_to+"--"+element.name+"--"+element.date_from);
                // console.log("approvedEventUser=e=");
                // let date_to = this.datePipe.transform(element.date_to, 'yyyy-MM-dd');
                // let date_from = this.datePipe.transform(element.date_from, 'yyyy-MM-dd');
                let date_to = element.date_to.replace('Z', '')
                let date_from = element.date_from.replace('Z', '');
                // console.log(date_to+"-+*+-"+date_from);
                // console.log("this.todays_date-"+this.todays_date);
                // if (this.todays_date <= date_to && this.todays_date >= date_from) {
                if (this.todays_date <= date_to.split('T')[0] && this.todays_date >= date_from.split('T')[0]) {
                  // let dateDiffrance = this.dateDiffrance(this.todays_date, date_to);
                  let dateDiffrance = this.dateDiffrance(this.todays_date, date_to.split('T')[0]);
                  for (let i = 0; i < dateDiffrance.length; i++) {
                    if (dateDiffrance[i] == this.todays_date) {
                      this.currentEvent[ccount] = element;
                      this.currentEventList[ccount] = element;
                      ccount++
                    }
                  }
                }
                else {
                  //To eleminate the past events Currently not applied
                  if (this.todays_date >= date_from) {
                    this.pastEvent[pcount] = element;
                    pcount++;
                  }
                  else {
                    this.upcomingEvent[ucount] = element;
                    this.upcomingEventList[ucount] = element;
                    ucount++;
                  }
                }
              }
            }
            this.authService.setLoader(false);
            // console.log("this.allData = = ");
            // console.log(this.allData);
            // console.log("this.currentEvent = = ");
            // console.log(this.currentEvent);
            // console.log("this.upcomingEvent = = ");
            // console.log(this.upcomingEvent);
            // console.log("this.pastEvent = = ");
            // console.log(this.pastEvent);

            if (this.currentEvent.length) {
              this.currentEventLength = false;
            }

            if (this.upcomingEvent.length) {
              this.upcomingEventLength = false;
            }

            // this.getAuthorName();
            this.getCalendarData();
          }
        );
    }
  }

  dateDiffrance(startDate, endDate) {
    const listDate = [];
    const dateMove = new Date(startDate);
    let strDate = startDate;
    while (strDate <= endDate) {
      strDate = dateMove.toISOString().slice(0, 10);
      listDate.push(strDate);
      dateMove.setDate(dateMove.getDate() + 1);
    };
    let popped = listDate.pop();
    return listDate;
  }

  /* getAuthor(auth_id) {
    //console.log("Organizer Auth");
    this.authService.memberSendRequest('get', 'singleUserdetails/' + auth_id, auth_id)
      .subscribe(
        (respData: JSON) => {
          this.authService.setLoader(false);
          let userData = respData[0]['users'][0];
          //console.log("userData GET AUTHOR");
          //console.log(userData);
          //console.log(userData.firstname);
          let auth_name: string = userData.firstname + " " + userData.lastname;
          //console.log("name " + auth_name);
          this.name = auth_name;
          this.auth_name = auth_name;
          return auth_name;
        }
      );
  }

  getAuthorName() {
    //console.log("AUTHOR NAME");
    //console.log(this.allData);
    this.allData.forEach((currentValue, index) => {
      //console.log(currentValue.author + currentValue.name);
      // if (currentValue.author) {
        // this.allData.splice(index, 1);
        this.authService.memberSendRequest('get', 'singleUserdetails/' + currentValue.author, currentValue.author)
          .subscribe(
            (respData: JSON) => {
              this.authService.setLoader(false);
              let userData = respData[0]['users'][0];
              //console.log("userData GET AUTHOR");
              //console.log(userData);
              let auth_name: string = userData.firstname + " " + userData.lastname;
              //console.log("name " + auth_name);
              this.name = auth_name;
              this.auth_name = auth_name;
              // return auth_name;
            }
          );
      // }
    });
    //console.log("this.allData");
    //console.log(this.allData);
  } */

  isTodayEvents: boolean = true;
  isUpcomingEvents: boolean = false;

  onTodayEvents() {
    this.isTodayEvents = true;
    this.isUpcomingEvents = false;
  }

  onUpcomingEvents() {
    this.isTodayEvents = false;
    this.isUpcomingEvents = true;
  }

  getCalendarData() {
    // console.log('------------------------------')
    // console.log('ALLL DATA');
    // console.log(this.allData);
    // console.log('-----------------------------');
    var count = 0;
    var pastData = this.pastEvent;
    var upcomingData = this.upcomingEvent;
    var todayData = this.currentEvent;
    //join the 2 array and send to Events
    this.allData.forEach((keys: any, vals: any) => {
      /* var date_from:any = this.datePipe.transform(keys.date_from, "yyyy-MM-ddTHH:mm:ss");
      var date_to:any = this.datePipe.transform(keys.date_to, "yyyy-MM-ddTHH:mm:ss"); */
      let date_from = keys.date_from.replace('Z', '');
      let date_to = keys.date_to.replace('Z', '');
      // this.calendarEvents[count] = { 'title': keys.name, 'start': keys.date_from, 'end': keys.date_to, 'description': keys.description, 'event_id': keys.id,  'type': keys.type };
      this.calendarEvents[count] = { 'title': keys.name, 'start': date_from, 'end': date_to, 'description': keys.description, 'event_id': keys.id, 'type': keys.type, 'classNames': this.eventTypeList[keys.type].class };
      count++;
    });
    console.log("this.calendarEvents");
    console.log(this.calendarEvents);
  }

  onDateClick(res) {
    console.log("onDAteClick");
    console.log(res);
    for (let index = 0; index < res.dayEl.parentElement.parentElement.childElementCount; index++) {
      let element = res.dayEl.parentElement.parentElement.children[index].children;
      for (let i = 0; i < element.length; i++) {
        let elClass = element[i].classList;
        element[i].classList.remove("grid-active");
      }
    }

    if (this.datePipe.transform(res.date, "yyyy-MM-dd") == res.dayEl.attributes[1].textContent) {
      ////console.log("insideIFF");
      res.dayEl.classList.add("grid-active");
      ////console.log(res.dayEl.classList);
    }

    this.calendarClicked = true;
    var count = 0;
    this.clickedEventData = [];
    ////console.log("clickedDate-> " + this.datePipe.transform(res.date, "yyyy-MM-dd"));
    ////console.log(this.calendarEvents)
    this.calendarEvents.forEach((keys: any, vals: any) => {
      if (res.dateStr >= this.datePipe.transform(keys.start, "yyyy-MM-dd") && res.dateStr <= this.datePipe.transform(keys.end, "yyyy-MM-dd")) {
        ////console.log("this DATE");
        ////console.log(keys.start + " title-> " + keys.title + " desc-> " + keys.description + " event_id-> " + keys.event_id);
        this.clickedEventData[count] = { 'title': keys.title, 'start': keys.start, 'end': keys.end, 'description': keys.description, 'event_id': keys.event_id, 'type': keys.type, 'display': 'background'  };
        count++;
      }
    });
    ////console.log("this.clickedEventData -> ");
    ////console.log(this.clickedEventData);
  }

  handleEventClick(res){ 
    this.calendarEventClicked = true;
    // this.newClickedEvent.push(res.event);
    this.newClickedEvent = res.event;
    console.log("handleEventClick");
    console.log(res);
    console.log(res.event);
    console.log(this.newClickedEvent.classNames[0]);
    console.log(this.eventTypeList[this.newClickedEvent.extendedProps.type].name);
    $('#exampleModalLabel').text(this.eventTypeList[this.newClickedEvent.extendedProps.type].name);
    $('#showPopup').trigger('click');
    //console.log('event description->',res.event.description);
  }

  closeModal() {
    console.log("CLOSE");
    $('#showPopup').trigger('click');
  }

  viewDetails() {
    console.log("View Details");
    $('#showPopup').trigger('click');
    this.router.navigate(['/event-detail/'+this.newClickedEvent.extendedProps.event_id]);
  }

  eventFilter() {
    //console.log("EVENT FILTER");
    //console.log("EVENT FILTER selcted"+$('#filter_events').val());
    //console.log("====================================");
    var todayEventToShow = [];
    var upcomingEventToShow = [];
    this.currentEvent.forEach(function (value, key) {
      if (value.type == $('#filter_events').val()) {
        todayEventToShow.push(value);
      }
    });
    
    this.upcomingEvent.forEach(function (value, key) {
      if (value.type == $('#filter_events').val()) {
        upcomingEventToShow.push(value);
      }
    });

    if (todayEventToShow.length) {
      this.currentEventList.splice(0, this.currentEventList.length);
      todayEventToShow.forEach((val, key) => {
        this.currentEventList.push(val);
      });
    }
    else{
      this.currentEventList.splice(0, this.currentEventList.length);
    }

    if(upcomingEventToShow.length){
      this.upcomingEventList.splice(0, this.upcomingEventList.length);
      upcomingEventToShow.forEach((val, key) => {
        this.upcomingEventList.push(val);
      });
    }
    else{
      this.upcomingEventList.splice(0, this.upcomingEventList.length);
    }

    if ($('#filter_events').val() == "0") {
      //Show all dropdown
      this.currentEventList.splice(0, this.currentEventList.length);
      this.upcomingEventList.splice(0, this.upcomingEventList.length);
      this.currentEvent.forEach((val, key) => {
        this.currentEventList.push(val);
      });
      this.upcomingEvent.forEach((val, key) => {
        this.upcomingEventList.push(val);
      });
    }
    //console.log("currentEventList FILTER");
    //console.log(this.currentEventList);

    //console.log("upcomingEventToShow FILTER");
    //console.log(this.upcomingEventList);

    //console.log("C EVENTS");
    //console.log(this.currentEvent);
    //console.log("U EVENTS");
    //console.log(this.upcomingEvent);
  }
}