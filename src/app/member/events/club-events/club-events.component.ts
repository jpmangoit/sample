import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { DatePipe } from '@angular/common';
import { LanguageService } from '../../../service/language.service';

@Component({
  selector: 'app-club-events',
  templateUrl: './club-events.component.html',
  styleUrls: ['./club-events.component.css'],
  providers: [DatePipe]
})
export class ClubEventsComponent implements OnInit {

  language;
  all_events;
  currentEvent = [];
  currentEventLength:boolean = true;
  upcomingEvent = [];
  upcomingEventLength:boolean = true;
  pastEvent = [];
  date;
  todays_date;
  eventTypeList:any = [];
  constructor(
    private authService: AuthServiceService,
    private datePipe: DatePipe,
    private router: Router,
    private lang : LanguageService
  ) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('token')) {
      this.language = this.lang.getLanguaageFile();
      this.eventTypeList[1] = {name: this.language.create_event.club_event, class: "club-event-color"};
      this.eventTypeList[2] = {name: this.language.create_event.group_event,  class: "group-event-color"};
      this.eventTypeList[3] = {name: this.language.create_event.functionaries_event,  class: "functionaries-event-color"};
      this.eventTypeList[4] = {name: this.language.create_event.courses,  class: "courses-event-color"};
      this.eventTypeList[5] = {name: this.language.create_event.seminar,  class: "seminar-event-color"};

      this.currentEvent = [];
      this.upcomingEvent = [];
      let userId = localStorage.getItem('user-id');
      this.authService.setLoader(true);
      this.authService.memberSendRequest('get', 'approvedEvents/user/'+userId, null)
        .subscribe(
          (respData: JSON) => {
            this.authService.setLoader(false);
            this.all_events = respData;
            this.date = new Date(); // Today's date            
            this.todays_date = this.datePipe.transform(this.date, 'yyyy-MM-dd');
            var element = null;
            var ccount = 0;
            var ucount = 0;
            var pcount = 0;           
            for (var key in respData) {
              if (respData.hasOwnProperty(key)) {
                element = respData[key];
                let date_to = this.datePipe.transform(element.date_to, 'yyyy-MM-dd');
                let date_from = this.datePipe.transform(element.date_from, 'yyyy-MM-dd');
                if (this.todays_date <= date_to && this.todays_date >= date_from) {
                  let dateDiffrance =  this.dateDiffrance(this.todays_date,date_to);
                  console.log(dateDiffrance)
                  for(let i = 0; i < dateDiffrance.length; i++){ 
                    if (dateDiffrance[i] == this.todays_date){
                      this.currentEvent[ccount] = element;
                      ccount++
                    }              
                  }
                }
                else {
                  //To eleminate the past events Currently not applied
                  if (this.todays_date >= date_from) {
                    this.pastEvent[pcount] = element;
                    pcount++;
                  }
                  else {
                    this.upcomingEvent[ucount] = element;
                    ucount++;
                  }
                }
              }
            }
            //console.log("this.currentEvent = = ");
            //console.log(this.currentEvent);
            if(this.currentEvent.length){
              this.currentEventLength = false;
            }
            //console.log("this.upcomingEvent = = ");
            //console.log(this.upcomingEvent);
            if(this.upcomingEvent.length){
              this.upcomingEventLength = false;
            }
          }
        );
    }
  }

  // upEvent(element,date){
  //let date = dateDiffrance[i]+'T'+element.date_from.split('T')[1];
  //this.upEvent(element,date);
  //   console.log('-----inin---'+date)
  //   let test = {}
  //   test = element;
  //   //delete test['date_from'];
  //   test['date_from_new'] = date;
  //   console.log(test)
  //   //this.upcomingEvent.push(element);
  //   //console.log(this.upcomingEvent)
  // }

  dateDiffrance(startDate,endDate){
    const listDate = [];
    const dateMove = new Date(startDate);
    let strDate = startDate;
    while (strDate <= endDate) {
      strDate = dateMove.toISOString().slice(0, 10);
      listDate.push(strDate);
      dateMove.setDate(dateMove.getDate() + 1);
    };
    let popped = listDate.pop();
    return listDate;
  }

  redirectCalendar(){
    var self = this;
    self.router.navigate(['organizer']);
  }

}
