import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthServiceService } from '../../../service/auth-service.service';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';
import { Location } from '@angular/common';
import { LanguageService } from '../../../service/language.service';
declare var $: any;

@Component({
	selector: 'app-event-detail',
	templateUrl: './event-detail.component.html',
	styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {
	language;
	eventDetails: any = null;
	imageurl: any = null;
	showImage = false;
	organizerDetails: any = [];
	unapprovedParticipants: any = [];
	approvedParticipants: any = [];

	constructor(
		private authService: AuthServiceService,
		private router: Router,
		private route: ActivatedRoute,
		private _location: Location,
		private confirmDialogService: ConfirmDialogService,
		private lang: LanguageService
	) { }

	ngOnInit(): void {
		this.language = this.lang.getLanguaageFile();
		this.route.params.subscribe(params => {
			const eventid = params['eventid'];
			this.getEventDetails(eventid);
		});
	}

	goBack() {
		this._location.back();
	}

	getEventDetails(eventid) {
		if (sessionStorage.getItem('token')) {
			this.authService.setLoader(true);
			var user_id = localStorage.getItem('user-id');
			this.authService.memberSendRequest('get', 'get-event-by-id/' + eventid, null)
				.subscribe(
					(respData: JSON) => {
						this.authService.setLoader(false);
						console.log(respData['result'][0]);
						this.eventDetails = respData['result'][0];
						if (this.eventDetails) {
							//console.log(this.eventDetails.picture_video.length);
							var url = this.eventDetails.picture_video.split('\"');
							//console.log(url[1]);
							//console.log(url.length);
							if(url.length > 1){
								this.imageurl = url[1];
								this.showImage = true;
							}
							else{
								this.imageurl = null;
								this.showImage = false;
							}
							//console.log(this.showImage);
							this.getOrganizerDetails(eventid);
							this.getParticipantDetails(eventid);
						}
					}
				);
		}
	}

	getOrganizerDetails(eventid) {
		if (sessionStorage.getItem('token')) {
			this.authService.setLoader(true);
			var user_id = localStorage.getItem('user-id');
			this.authService.memberSendRequest('get', 'approvedParticipants/event/' + eventid, null)
				.subscribe(
					(respData: JSON) => {
						this.authService.setLoader(false);
						console.log("Organizer Details");
						console.log(respData);
						for (const key in respData) {
							if (Object.prototype.hasOwnProperty.call(respData, key)) {
								const element = respData[key];
								if (this.eventDetails.author == element.id) {
									// this.organizerDetails = element;
									this.organizerDetails.push(element);
								}
								else {
									// this.approvedParticipants = element;
									this.approvedParticipants.push(element);
								}
							}
						}
						// console.log("Approved Participants");
						// console.log(this.approvedParticipants);
						// console.log("Organizer Participants");
						// console.log(this.organizerDetails);
					}
				);
		}
	}

	getParticipantDetails(eventid) {
		if (sessionStorage.getItem('token')) {
			this.authService.setLoader(true);
			var user_id = localStorage.getItem('user-id');
			this.authService.memberSendRequest('get', 'unapprovedParticipants/event/' + eventid, null)
				.subscribe(
					(respData: JSON) => {
						this.authService.setLoader(false);
						// console.log("Participant Details");
						// console.log(respData);
						this.unapprovedParticipants = respData;
					}
				);
		}
		console.log("Unapproved Participants");
		console.log(this.unapprovedParticipants);
	}

	showToggle: boolean = false;
	onShow() {
		let el = document.getElementsByClassName("bunch_drop");
		if (!this.showToggle) {
			this.showToggle = true;
			el[0].className = "bunch_drop show";
		}
		else {
			this.showToggle = false;
			el[0].className = "bunch_drop";
		}
	}

	deleteEvents(eventId) {
		let self = this;
		this.confirmDialogService.confirmThis(this.language.confirmation_message.delete_event, function () {
			self.authService.setLoader(true);
			self.authService.memberSendRequest('delete', 'event/' + eventId, null)
				.subscribe(
					(respData: JSON) => {
						self.authService.setLoader(false);
						const url: string[] = ["/organizer"];
						self.router.navigate(url);
					}
				)
		}, function () {
			console.log("No clicked");
			$('.dropdown-toggle').trigger('click');
		})
	}

}
