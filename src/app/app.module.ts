import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { RecoverPasswordComponent } from './pages/recover-password/recover-password.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './member/dashboard/dashboard.component';
import { HeaderComponent } from './common/header/header.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { MenuComponent } from './common/menu/menu.component';
import { LayoutComponent } from './common/layout/layout.component';
import { ClubNewsComponent } from './member/news/club-news/club-news.component';
import { ClubAllNewsComponent } from './member/news/club-all-news/club-all-news.component';
import { ClubNewsDetailsComponent } from './member/news/club-news-details/club-news-details.component';
import { ClubDatesComponent } from './member/club-dates/club-dates.component';
import { ClubEventsComponent } from './member/events/club-events/club-events.component';
import { ClubAppointmentsComponent } from './member/club-appointments/club-appointments.component';
import { AuthServiceService } from './service/auth-service.service';
import { ClubWallComponent } from './member/club-wall/club-wall.component';
import { GroupNewsComponent } from './member/news/group-news/group-news.component';
import { BirthdaysComponent } from './member/birthdays/birthdays.component';
import { AuthGuard } from './auth.guard';
import { LimitTextPipe } from './pipe/limit-text.pipe';
import { CommunityComponent } from './member/community/community.component';
import { CommunityMessagesComponent } from './member/messages/community-messages/community-messages.component';
import { CommunityGroupsComponent } from './member/groups/community-groups/community-groups.component';
import { OrganizerComponent } from './member/organizer/organizer.component';
import { OrganizerEventComponent } from './member/events/organizer-event/organizer-event.component';
import { OrganizerTaskComponent } from './member/tasks/organizer-task/organizer-task.component';
import { OrganizerDocumentComponent } from './member/documents/organizer-document/organizer-document.component';
import { ProfileComponent } from './member/profiles/profile/profile.component';
import { ProfileViewComponent } from './member/profiles/profile-view/profile-view.component';
import { ProfileEditComponent } from './member/profiles/profile-edit/profile-edit.component';
import { ProfileBankComponent } from './member/profiles/profile-bank/profile-bank.component';
import { CreateEventComponent } from './member/events/create-event/create-event.component';
import { CreateTaskComponent } from './member/tasks/create-task/create-task.component';
import { CreateNewsComponent } from './member/news/create-news/create-news.component';
import { CreateGroupComponent } from './member/groups/create-group/create-group.component';
import { CreateMessageComponent } from './member/messages/create-message/create-message.component';
import { NgxPaginationModule} from 'ngx-pagination';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ProfileClubComponent } from './member/profiles/profile-club/profile-club.component';
import { UpdateNewsComponent } from './member/news/update-news/update-news.component';
import { UpdateEventComponent } from './member/events/update-event/update-event.component';
import { UpdateGroupComponent } from './member/groups/update-group/update-group.component';
import { UpdateTaskComponent } from './member/tasks/update-task/update-task.component';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';  
import {ConfirmDialogService} from './confirm-dialog/confirm-dialog.service';
import { ImageViewerComponent } from './member/image-viewer/image-viewer.component';
import { GroupDetailComponent } from './member/groups/group-detail/group-detail.component';
import { EventDetailComponent } from './member/events/event-detail/event-detail.component';
import { OrganizerAllTaskComponent } from './member/tasks/organizer-all-task/organizer-all-task.component';
import { OrganizerPersonalTaskComponent } from './member/tasks/organizer-personal-task/organizer-personal-task.component';
import { OrganizerGroupTaskComponent } from './member/tasks/organizer-group-task/organizer-group-task.component';
import { OrganizerCreatedTaskComponent } from './member/tasks/organizer-created-task/organizer-created-task.component';
import { ProfileMyClubComponent } from './member/profiles/profile-my-club/profile-my-club.component';

import { LanguageService } from './service/language.service';

import { FullCalendarModule } from '@fullcalendar/angular'; 
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';

import { NgxDocViewerModule } from 'ngx-doc-viewer';

import { ProfileBankEditComponent } from './member/profiles/profile-bank-edit/profile-bank-edit.component';

import { GroupMessagesComponent } from './member/messages/group-messages/group-messages.component';
import { ClubMessagesComponent } from './member/messages/club-messages/club-messages.component';
import { PersonalMessagesComponent } from './member/messages/personal-messages/personal-messages.component';
import { MyDocumentComponent } from './member/documents/my-document/my-document.component';
import { ClubDocumentComponent } from './member/documents/club-document/club-document.component';
import { ArchivedDocumentComponent } from './member/documents/archived-document/archived-document.component';
import { CurrentStatusDocumentComponent } from './member/documents/current-status-document/current-status-document.component';
import { AllDocumentsComponent } from './member/documents/all-documents/all-documents.component';
import { TaskDetailComponent } from './member/tasks/task-detail/task-detail.component';
import { TooltipDirective } from './tooltip.directive';
import { CreateChatComponent } from './member/create-chat/create-chat.component';

FullCalendarModule.registerPlugins([ 
  interactionPlugin,
  dayGridPlugin,
  timeGridPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RecoverPasswordComponent,
    DashboardComponent,
    HeaderComponent,
    PageNotFoundComponent,
    MenuComponent,
    LayoutComponent,
    ClubNewsComponent,
    ClubAllNewsComponent,
    ClubNewsDetailsComponent,
    ClubDatesComponent,
    ClubEventsComponent,
    ClubAppointmentsComponent,
    ClubWallComponent,
    GroupNewsComponent,
    BirthdaysComponent,
    LimitTextPipe,
    CommunityComponent,
    CommunityMessagesComponent,
    CommunityGroupsComponent,
    OrganizerComponent,
    OrganizerEventComponent,
    OrganizerTaskComponent,
    OrganizerDocumentComponent,
    ProfileComponent,
    ProfileViewComponent,
    ProfileEditComponent,
    ProfileBankComponent,
    CreateEventComponent,
    CreateTaskComponent,
    CreateNewsComponent,
    UpdateNewsComponent,
    UpdateEventComponent,
    UpdateGroupComponent,
    UpdateTaskComponent,
    CreateMessageComponent,
    ProfileClubComponent,
    ConfirmDialogComponent,
    ImageViewerComponent,
    CreateGroupComponent,
    GroupDetailComponent,
    EventDetailComponent,
    OrganizerAllTaskComponent,
    OrganizerPersonalTaskComponent,
    OrganizerGroupTaskComponent,
    OrganizerCreatedTaskComponent,
    ProfileMyClubComponent,
    ProfileBankEditComponent,
    GroupMessagesComponent,
    ClubMessagesComponent,
    PersonalMessagesComponent,
    MyDocumentComponent,
    ClubDocumentComponent,
    ArchivedDocumentComponent,
    CurrentStatusDocumentComponent,
    AllDocumentsComponent,
    TaskDetailComponent,
    TooltipDirective,
    CreateChatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
		ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularEditorModule,
    FullCalendarModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxPaginationModule,
    NgxDocViewerModule
  ],
  exports: [  
    ConfirmDialogComponent  
  ],  
  providers: [AuthServiceService,LanguageService, AuthGuard,ConfirmDialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
