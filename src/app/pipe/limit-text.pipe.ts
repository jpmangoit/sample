import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitText'
})
export class LimitTextPipe implements PipeTransform {

  transform(value: string, args: string): unknown {
    let limit = args ? parseInt(args, 10) : 10;
    let trail = '...';

    console.log(value);
    // return value.length > limit ? value.substring(0, limit) + trail : value;
    var v = value.length > limit ? value.substring(0, limit) + trail : value;
    console.log("PIPEE");
    console.log(v);
    console.log("PIPEE");
    return v;
  }

}
